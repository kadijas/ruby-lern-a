require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module RubyOnRailsA
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
	# config.i18n.default_locale = :de
	
	# https://guides.rubyonrails.org/v4.0/configuring.html
	# section 3.3
	#	- generate js instead of coffee
	#	- skip helpers and css
	# puts "Configuring Generators now..."
	config.generators do |g|
	  g.javascript_engine :js
	  g.helper false
	  g.stylesheets false
	  g.javascripts true	# turn this on only for those views that need scripts
	end

	# Try, is this option honored in 4.1.6
	config.active_record.schema_format = :sql
	
  end
end
