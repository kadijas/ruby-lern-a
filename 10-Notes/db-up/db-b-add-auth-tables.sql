/*
Database Create Script for 'appts'

SOURCE LOCATIONs: Combines content from
	$/30-Design/310-DB/3110-SQL/db-21-add-auth-tables.sql
	$/30-Design/310-DB/3110-SQL/db-25a-form-tables.sql
	$/30-Design/310-DB/3110-SQL/db-30b-add-authz-views.sql

TODO:
1. Always -
  Change line
  CREATE TABLE staffs (
  to
  CREATE TABLE IF NOT EXISTS staffs (

*/
CREATE TABLE gen_actions (
  action_id   INT UNSIGNED NOT NULL AUTO_INCREMENT,
  feature_id  INT UNSIGNED NOT NULL,
  action_code varchar(20) NOT NULL UNIQUE comment 'Extra long code field to guarantee uniqueness. Will be a combination of controller and Action names. TODO: Use generated code.',
  action_name varchar(60) NOT NULL UNIQUE comment 'Combination of Controller, Action and HTTP Method.',
  action_desc varchar(255),
  status      int(11) DEFAULT 0 NOT NULL,
  add_by      INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on      timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by     INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on     timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (action_id),
  CONSTRAINT `UNIQ_FEATURE_ACTION-CODES`
    UNIQUE (action_id, feature_id)) comment='Listing of all actions available for each feature in the system.';
CREATE TABLE gen_features (
  feature_id   INT UNSIGNED NOT NULL AUTO_INCREMENT,
  feature_code varchar(20) NOT NULL,
  feature_name varchar(60) NOT NULL UNIQUE,
  feature_desc varchar(255),
  status       int(11) DEFAULT 0 NOT NULL,
  add_by       INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on       timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by      INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on      timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (feature_id),
  UNIQUE INDEX (feature_code)) comment='Listing of all available features (Controllers) in the system.';
CREATE TABLE gen_roles (
  role_id   INT UNSIGNED NOT NULL AUTO_INCREMENT,
  role_name varchar(60) NOT NULL,
  role_desc varchar(255) NOT NULL,
  status    int(11) DEFAULT 0 NOT NULL,
  add_by    INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on    timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by   INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on   timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (role_id)) comment='Roles in the system that can be assigned to a user, for managing system access.';
CREATE TABLE gen_ui_sections (
  uis_id     INT UNSIGNED NOT NULL AUTO_INCREMENT,
  action_id  INT UNSIGNED NOT NULL,
  feature_id INT UNSIGNED NOT NULL,
  uis_code   varchar(20) NOT NULL comment 'Make this human readable, to be used directly in code.',
  uis_name   varchar(60) NOT NULL UNIQUE,
  uis_desc   varchar(255),
  status     int(11) DEFAULT 0 NOT NULL,
  add_by     INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on     timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by    INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on    timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (uis_id,
  action_id),
  UNIQUE INDEX (uis_code)) comment='List of all non-default UI Sections for actions (View) of each feature (Controller) in the system.';
CREATE TABLE MAINT_SESSIONS (
  MSID       INT UNSIGNED NOT NULL,
  TOKEN      varchar(60) NOT NULL comment 'Token shared with client, and will be used to match request to session.',
  DEVICE_SIG text comment 'Device identifying hash, to uniquely link a device to a session. Helps avoid session hijacks.',
  STAFF_ID   int(11) NOT NULL comment 'Link to system ID of the user.',
  ROLE       int(11) DEFAULT -1 NOT NULL comment 'Not of significance for now, as users can be in multiple roles simultaneously.',
  ISSUE_TIME timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  LAST_USED  timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  STATUS     int(11) NOT NULL comment 'Current status of the session. Link to values from enum ''x-y-z''',
  PAYLOAD    text comment 'JSON formatted profile data of the current user. To minimize db calls.',
  PRIMARY KEY (MSID));
CREATE TABLE IF NOT EXISTS staffs (
  id              int(11) NOT NULL AUTO_INCREMENT,
  fn              varchar(255),
  ln              varchar(255),
  mi              varchar(255),
  pn              varchar(255),
  title           tinyint(4),
  addone          varchar(255),
  addtwo          varchar(255),
  city            varchar(255),
  state           varchar(255),
  zip             varchar(255),
  ext             varchar(255),
  mphone          varchar(255),
  email           varchar(255),
  ssn             varchar(255),
  created_by      int(11),
  created_at      datetime DEFAULT CURRENT_TIMESTAMP NULL,
  updated_by      int(11),
  updated_at      datetime DEFAULT CURRENT_TIMESTAMP NULL,
  ststatus        tinyint(4) DEFAULT '1',
  staffnum        varchar(20),
  ophone          varchar(255),
  password_digest varchar(255),
  is_user         tinyint(4),
  is_prov         tinyint(4) DEFAULT '0',
  is_rm           tinyint(4) DEFAULT '0',
  is_hyg          tinyint(4) DEFAULT '0',
  PRIMARY KEY (id)) AUTO_INCREMENT = 18 DEFAULT CHARSET = utf8;
CREATE TABLE tpl_role_auth_map (
  role_id     INT UNSIGNED NOT NULL,
  action_id   INT UNSIGNED NOT NULL,
  feature_id  INT UNSIGNED NOT NULL,
  action_name varchar(60) NOT NULL,
  status      int(11) DEFAULT 0 NOT NULL,
  add_by      INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on      timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by     INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on     timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (role_id,
  action_id)) comment='Template for default mapping of features and actions assigned to a role.';
CREATE TABLE user_auth_map (
  staff_id    int(11) NOT NULL,
  action_id   INT UNSIGNED NOT NULL,
  feature_id  INT UNSIGNED NOT NULL,
  action_name varchar(60) NOT NULL comment 'Combination of Controller, Action and HTTP Method. To enable dynamic query of authorizations.',
  status      int(11) DEFAULT 0 NOT NULL,
  add_by      INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on      timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by     INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on     timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (staff_id,
  action_id)) comment='Mapping of all actions authorized for a user in the system.';
CREATE TABLE user_role_map (
  staff_id   int(11) NOT NULL,
  role_id    INT UNSIGNED NOT NULL,
  is_primary int(4) DEFAULT 0 NOT NULL comment 'Flag to indicate if this is the primary role of the user, or additional role. 0 = Addl, 1 = Primary.',
  status     int(11) DEFAULT 0 NOT NULL,
  add_by     INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on     timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by    INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on    timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (staff_id,
  role_id)) comment='Mapping for Users and roles. Identifies all roles a user has been assigned to in the system.';
CREATE TABLE user_uis_blacklist_map (
  staff_id   int(11) NOT NULL,
  uis_id     INT UNSIGNED NOT NULL,
  action_id  INT UNSIGNED NOT NULL,
  feature_id INT UNSIGNED NOT NULL,
  uis_code   varchar(20) NOT NULL UNIQUE comment 'Text code more readable in code than numeric code. To enable dynamic query of authorizations.',
  status     int(11) DEFAULT 0 NOT NULL,
  add_by     INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on     timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by    INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on    timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (staff_id,
  uis_id,
  action_id)) comment='List of all blacklisted UI Sections of Actions (Views) for a user. These will be used to block rendering of those sections in the UI. And possibly in the back-end to block data from the blacklisted sections.';
CREATE UNIQUE INDEX user_auth_map
  ON user_auth_map (staff_id, action_name);
CREATE UNIQUE INDEX user_uis_blacklist_map
  ON user_uis_blacklist_map (staff_id, uis_code);
ALTER TABLE tpl_role_auth_map ADD CONSTRAINT FKtpl_role_a379612 FOREIGN KEY (role_id) REFERENCES gen_roles (role_id);
ALTER TABLE tpl_role_auth_map ADD CONSTRAINT FKtpl_role_a325011 FOREIGN KEY (action_id) REFERENCES gen_actions (action_id);
ALTER TABLE user_role_map ADD CONSTRAINT FKuser_role_321036 FOREIGN KEY (role_id) REFERENCES gen_roles (role_id);
ALTER TABLE gen_actions ADD CONSTRAINT FKgen_action525764 FOREIGN KEY (feature_id) REFERENCES gen_features (feature_id);
ALTER TABLE gen_ui_sections ADD CONSTRAINT FKgen_ui_sec153353 FOREIGN KEY (action_id) REFERENCES gen_actions (action_id);
ALTER TABLE user_uis_blacklist_map ADD CONSTRAINT FKuser_uis_b270849 FOREIGN KEY (uis_id, action_id) REFERENCES gen_ui_sections (uis_id, action_id);
ALTER TABLE user_auth_map ADD CONSTRAINT FKuser_auth_732366 FOREIGN KEY (action_id) REFERENCES gen_actions (action_id);
ALTER TABLE user_role_map ADD CONSTRAINT FKuser_role_94996 FOREIGN KEY (staff_id) REFERENCES staffs (id);
ALTER TABLE user_auth_map ADD CONSTRAINT FKuser_auth_950632 FOREIGN KEY (staff_id) REFERENCES staffs (id);
ALTER TABLE user_uis_blacklist_map ADD CONSTRAINT FKuser_uis_b160864 FOREIGN KEY (staff_id) REFERENCES staffs (id);
ALTER TABLE tpl_role_auth_map ADD CONSTRAINT FKtpl_role_a798404 FOREIGN KEY (feature_id) REFERENCES gen_features (feature_id);
ALTER TABLE gen_ui_sections ADD CONSTRAINT FKgen_ui_sec970062 FOREIGN KEY (feature_id) REFERENCES gen_features (feature_id);
ALTER TABLE user_auth_map ADD CONSTRAINT FKuser_auth_792099 FOREIGN KEY (action_name) REFERENCES gen_actions (action_name);
ALTER TABLE user_uis_blacklist_map ADD CONSTRAINT FKuser_uis_b326039 FOREIGN KEY (feature_id) REFERENCES gen_features (feature_id);
ALTER TABLE user_uis_blacklist_map ADD CONSTRAINT FKuser_uis_b675482 FOREIGN KEY (uis_code) REFERENCES gen_ui_sections (uis_code);
ALTER TABLE user_auth_map ADD CONSTRAINT FKuser_auth_115808 FOREIGN KEY (feature_id) REFERENCES gen_features (feature_id);
ALTER TABLE tpl_role_auth_map ADD CONSTRAINT FKtpl_role_a877886 FOREIGN KEY (action_name) REFERENCES gen_actions (action_name);
CREATE TABLE user_auths_form (
  action_id   INT UNSIGNED NOT NULL AUTO_INCREMENT,
  staff_id    int(11) NOT NULL,
  feature_id  INT UNSIGNED NOT NULL,
  action_code varchar(20) NOT NULL UNIQUE comment 'Extra long code field to guarantee uniqueness. Will be a combination of controller and Action names. TODO: Use generated code.',
  action_name varchar(60) NOT NULL UNIQUE comment 'Combination of Controller, Action and HTTP Method.',
  action_desc varchar(255),
  status_curr int(11) DEFAULT 0 NOT NULL comment 'Copy to track changed records',
  status_new  int(11) DEFAULT 0 NOT NULL comment 'Copy to track updated records',
  add_by      INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on      timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by     INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on     timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (action_id,
  staff_id),
  CONSTRAINT `UNIQ_FEATURE_ACTION-CODES`
    UNIQUE (action_id, feature_id)) comment='Entity to support standalone models, based on ''gen_actions''';
CREATE TABLE user_staff_form (
  staff_id  int(11) NOT NULL AUTO_INCREMENT,
  staffnum  varchar(20),
  full_name varchar(255),
  nickname  varchar(255),
  mphone    varchar(255),
  email     varchar(255),
  ssn       varchar(255),
  status    int(11) DEFAULT 0 NOT NULL,
  add_by    INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on    timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by   INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on   timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (staff_id)) comment='Entity to support standalone models, based on ''staffs''';
CREATE TABLE user_ui_blacklist_form (
  uis_id      INT UNSIGNED NOT NULL AUTO_INCREMENT,
  staff_id    int(11) NOT NULL,
  action_id   INT UNSIGNED NOT NULL,
  feature_id  INT UNSIGNED NOT NULL,
  uis_code    varchar(20) NOT NULL comment 'Make this human readable, to be used directly in code.',
  uis_name    varchar(60) NOT NULL UNIQUE,
  uis_desc    varchar(255),
  status_curr int(11) DEFAULT 0 NOT NULL comment 'Copy to track changed records',
  status_new  int(11) DEFAULT 0 NOT NULL comment 'Copy to track updated records',
  add_by      INT UNSIGNED DEFAULT 0 NOT NULL,
  add_on      timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
  edit_by     INT UNSIGNED DEFAULT 0 NOT NULL,
  edit_on     timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  PRIMARY KEY (uis_id,
  staff_id,
  action_id),
  UNIQUE INDEX (uis_code)) comment='Entity to support standalone models, based on ''gen_ui_sections''';
ALTER TABLE user_auths_form ADD CONSTRAINT FKuser_auths433222 FOREIGN KEY (staff_id) REFERENCES user_staff_form (staff_id);
ALTER TABLE user_ui_blacklist_form ADD CONSTRAINT FKuser_ui_bl485438 FOREIGN KEY (action_id, staff_id) REFERENCES user_auths_form (action_id, staff_id);

CREATE OR REPLACE VIEW `vw_actions_features` AS
    SELECT
        `gen_actions`.`action_id` AS `action_id`,
        `gen_actions`.`feature_id` AS `feature_id`,
        `gen_actions`.`action_code` AS `action_code`,
        `gen_actions`.`action_name` AS `action_name`,
        `gen_actions`.`action_desc` AS `action_desc`,
        `gen_actions`.`status` AS `status`,
        `gen_features`.`feature_code` AS `feature_code`,
        `gen_features`.`feature_name` AS `feature_name`,
        `gen_features`.`feature_desc` AS `feature_desc`,
        `gen_features`.`status` AS `feature_status`
    FROM
        (`gen_features`
        JOIN `gen_actions` ON ((`gen_features`.`feature_id` = `gen_actions`.`feature_id`)));
