-- From file 10-Notes/db-up/db-e-test-dbup.sql
--
-- Is this needed?
FLUSH TABLES;

-- SIMPLE TEST TO CHECK SCRIPT SUCCESS
SELECT TABLE_NAME,
       TABLE_ROWS AS TABLE_ROW_COUNT
FROM   information_schema.TABLES
WHERE  TABLE_SCHEMA = 'appts'
       AND TABLE_TYPE = 'BASE TABLE'
       AND TABLE_ROWS != 0
ORDER  BY 2 ASC;
