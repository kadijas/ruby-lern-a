https://github.com/composite-primary-keys/composite_primary_keys
https://dan.chak.org/enterprise-rails/chapter-8-composite-keys-and-domain-key-normal-form/

gem install composite_primary_keys
gem 'composite_primary_keys', '=7.0.13'

In config/environment.rb file:
require 'composite_primary_keys'

class Membership < ActiveRecord::Base
  self.primary_keys = :user_id, :group_id
  belongs_to :user
  belongs_to :group
  has_many :statuses, :class_name => 'MembershipStatus', :foreign_key => [:user_id, :group_id]
end

class MembershipStatus < ActiveRecord::Base
  belongs_to :membership, :foreign_key => [:user_id, :group_id]
end

Membership.find([1,1])  # composite ids returns single instance


Release versions
v7.0.13	2015-01-25
v7.0.12	2014-11-10
v7.0.11	2014-10-10
v7.0.16	2014-10-06
v7.0.14	2014-10-06
v7.0.10	2014-08-07
v7.0.0	2014-05-27

