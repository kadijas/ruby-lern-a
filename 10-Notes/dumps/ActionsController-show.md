# http://0.0.0.0:3000/actions/2

```json
{
	"action": {
		"action_id": 2,
		"feature_id": 2,
		"action_code": "53749d54a436",
		"action_name": "f_application-a_alogs-ge",
		"action_desc": "f_application-a_alogs-ge",
		"status": 1,
		"add_by": 0,
		"add_on": "2030-04-19T01:20:00.000Z",
		"edit_by": 0,
		"edit_on": "2030-04-19T01:20:00.000Z",
		"feature": {
			"feature_id": 2,
			"feature_code": "application",
			"feature_name": "application",
			"feature_desc": "TBD",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z"
		}
	}
}
```
