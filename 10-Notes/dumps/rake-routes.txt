               Prefix Verb   URI Pattern                           Controller#Action
                 root GET    /                                     welcome#index
        welcome_index GET    /welcome/index(.:format)              welcome#index
                roles GET    /roles(.:format)                      roles#index
                      POST   /roles(.:format)                      roles#create
             new_role GET    /roles/new(.:format)                  roles#new
            edit_role GET    /roles/:id/edit(.:format)             roles#edit
                 role GET    /roles/:id(.:format)                  roles#show
                      PATCH  /roles/:id(.:format)                  roles#update
                      PUT    /roles/:id(.:format)                  roles#update
                      DELETE /roles/:id(.:format)                  roles#destroy
             features GET    /features(.:format)                   features#index
                      POST   /features(.:format)                   features#create
          new_feature GET    /features/new(.:format)               features#new
         edit_feature GET    /features/:id/edit(.:format)          features#edit
              feature GET    /features/:id(.:format)               features#show
                      PATCH  /features/:id(.:format)               features#update
                      PUT    /features/:id(.:format)               features#update
                      DELETE /features/:id(.:format)               features#destroy
              actions GET    /actions(.:format)                    actions#index
                      POST   /actions(.:format)                    actions#create
           new_action GET    /actions/new(.:format)                actions#new
          edit_action GET    /actions/:id/edit(.:format)           actions#edit
               action GET    /actions/:id(.:format)                actions#show
                      PATCH  /actions/:id(.:format)                actions#update
                      PUT    /actions/:id(.:format)                actions#update
                      DELETE /actions/:id(.:format)                actions#destroy
    roleauthtemplates GET    /roleauthtemplates(.:format)          roleauthtemplates#index
                      POST   /roleauthtemplates(.:format)          roleauthtemplates#create
 new_roleauthtemplate GET    /roleauthtemplates/new(.:format)      roleauthtemplates#new
edit_roleauthtemplate GET    /roleauthtemplates/:id/edit(.:format) roleauthtemplates#edit
     roleauthtemplate GET    /roleauthtemplates/:id(.:format)      roleauthtemplates#show
                      PATCH  /roleauthtemplates/:id(.:format)      roleauthtemplates#update
                      PUT    /roleauthtemplates/:id(.:format)      roleauthtemplates#update
                      DELETE /roleauthtemplates/:id(.:format)      roleauthtemplates#destroy
    userauthmap_index GET    /userauthmap(.:format)                userauthmap#index
                      POST   /userauthmap(.:format)                userauthmap#create
      new_userauthmap GET    /userauthmap/new(.:format)            userauthmap#new
     edit_userauthmap GET    /userauthmap/:id/edit(.:format)       userauthmap#edit
          userauthmap GET    /userauthmap/:id(.:format)            userauthmap#show
                      PATCH  /userauthmap/:id(.:format)            userauthmap#update
                      PUT    /userauthmap/:id(.:format)            userauthmap#update
                      DELETE /userauthmap/:id(.:format)            userauthmap#destroy
                      POST   /userauthmap/:id(.:format)            userauthmap#update
