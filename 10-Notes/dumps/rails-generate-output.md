# Command Outputs for analysis

## 2019-05-09
```sh
rails generate model UserAuthsForm 				# maps to table user_auths_form
# Running via Spring preloader in process 8608
#       invoke  active_record
#       create    db/migrate/20190509084128_create_user_auths_forms.rb
#       create    app/models/user_auths_form.rb
#       invoke    test_unit
#       create      test/models/user_auths_form_test.rb
#       create      test/fixtures/user_auths_forms.yml

rails generate model UserForm 					# maps to table user_staff_form
# Running via Spring preloader in process 8692
#       invoke  active_record
#       create    db/migrate/20190509084258_create_user_forms.rb
#       create    app/models/user_form.rb
#       invoke    test_unit
#       create      test/models/user_form_test.rb
#       create      test/fixtures/user_forms.yml

rails generate model UserUiBlacklistEntryForm 	# maps to table user_ui_blacklist_form
# Running via Spring preloader in process 8744
#       invoke  active_record
#       create    db/migrate/20190509084327_create_user_ui_blacklist_entry_forms.rb
#       create    app/models/user_ui_blacklist_entry_form.rb
#       invoke    test_unit
#       create      test/models/user_ui_blacklist_entry_form_test.rb
#       create      test/fixtures/user_ui_blacklist_entry_forms.yml

rails generate model VwActionFeature 				# maps to view vw_actions_features
# Running via Spring preloader in process 12950
#       invoke  active_record
#       create    db/migrate/20190509105204_create_vw_action_features.rb
#       create    app/models/vw_action_feature.rb
#       invoke    test_unit
#       create      test/models/vw_action_feature_test.rb

```
