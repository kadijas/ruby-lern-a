# http://0.0.0.0:3000/actions

```json
[
	{
		"action": {
			"action_id": 1,
			"feature_id": 1,
			"action_code": "f681c7b88975",
			"action_name": "f_admin-a_index-ge",
			"action_desc": "f_admin-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 1,
				"feature_code": "admin",
				"feature_name": "admin",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 2,
			"feature_id": 2,
			"action_code": "53749d54a436",
			"action_name": "f_application-a_alogs-ge",
			"action_desc": "f_application-a_alogs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 2,
				"feature_code": "application",
				"feature_name": "application",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 3,
			"feature_id": 3,
			"action_code": "b7b9651b0d5b",
			"action_name": "f_appointment-a_addtprocs-ge",
			"action_desc": "f_appointment-a_addtprocs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 4,
			"feature_id": 3,
			"action_code": "1a16cf7391dd",
			"action_name": "f_appointment-a_create-ge",
			"action_desc": "f_appointment-a_create-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 5,
			"feature_id": 3,
			"action_code": "e5c0526fce95",
			"action_name": "f_appointment-a_edit-ge",
			"action_desc": "f_appointment-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 6,
			"feature_id": 3,
			"action_code": "8c7b1dc7358b",
			"action_name": "f_appointment-a_getappts-po",
			"action_desc": "f_appointment-a_getappts-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 7,
			"feature_id": 3,
			"action_code": "113456e43ed6",
			"action_name": "f_appointment-a_getopts-po",
			"action_desc": "f_appointment-a_getopts-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 8,
			"feature_id": 3,
			"action_code": "e89e4df78a06",
			"action_name": "f_appointment-a_getpatschedappts-ge",
			"action_desc": "f_appointment-a_getpatschedappts-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 9,
			"feature_id": 3,
			"action_code": "689e4ef21d65",
			"action_name": "f_appointment-a_index-ge",
			"action_desc": "f_appointment-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 10,
			"feature_id": 3,
			"action_code": "8b666022986e",
			"action_name": "f_appointment-a_remtprocs-ge",
			"action_desc": "f_appointment-a_remtprocs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 11,
			"feature_id": 3,
			"action_code": "b84dfc66ed2e",
			"action_name": "f_appointment-a_routeslip-ge",
			"action_desc": "f_appointment-a_routeslip-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 12,
			"feature_id": 3,
			"action_code": "023d0a73f434",
			"action_name": "f_appointment-a_rtslps-po",
			"action_desc": "f_appointment-a_rtslps-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 13,
			"feature_id": 3,
			"action_code": "f42c84dc9711",
			"action_name": "f_appointment-a_setconfirmst-ge",
			"action_desc": "f_appointment-a_setconfirmst-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 14,
			"feature_id": 3,
			"action_code": "5e19dda10595",
			"action_name": "f_appointment-a_update-po",
			"action_desc": "f_appointment-a_update-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 15,
			"feature_id": 3,
			"action_code": "6838f1c5cf33",
			"action_name": "f_appointment-a_updtmln-ge",
			"action_desc": "f_appointment-a_updtmln-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 3,
				"feature_code": "appointment",
				"feature_name": "appointment",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 16,
			"feature_id": 4,
			"action_code": "dec26286ca9b",
			"action_name": "f_calendar-a_index-ge",
			"action_desc": "f_calendar-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 4,
				"feature_code": "calendar",
				"feature_name": "calendar",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 17,
			"feature_id": 4,
			"action_code": "cfd9329428ac",
			"action_name": "f_calendar-a_schapt-po",
			"action_desc": "f_calendar-a_schapt-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 4,
				"feature_code": "calendar",
				"feature_name": "calendar",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 18,
			"feature_id": 4,
			"action_code": "c4ca9eeab759",
			"action_name": "f_calendar-a_utb-po",
			"action_desc": "f_calendar-a_utb-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 4,
				"feature_code": "calendar",
				"feature_name": "calendar",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 19,
			"feature_id": 5,
			"action_code": "65e43f1ad662",
			"action_name": "f_carrier-a_create-po",
			"action_desc": "f_carrier-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 5,
				"feature_code": "carrier",
				"feature_name": "carrier",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 20,
			"feature_id": 5,
			"action_code": "707d4663e827",
			"action_name": "f_carrier-a_edit-ge",
			"action_desc": "f_carrier-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 5,
				"feature_code": "carrier",
				"feature_name": "carrier",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 21,
			"feature_id": 5,
			"action_code": "cdef30df8fed",
			"action_name": "f_carrier-a_index-ge",
			"action_desc": "f_carrier-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 5,
				"feature_code": "carrier",
				"feature_name": "carrier",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 22,
			"feature_id": 5,
			"action_code": "4d16e93525d2",
			"action_name": "f_carrier-a_new-ge",
			"action_desc": "f_carrier-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 5,
				"feature_code": "carrier",
				"feature_name": "carrier",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 23,
			"feature_id": 5,
			"action_code": "83ec4c21f4d9",
			"action_name": "f_carrier-a_update-po",
			"action_desc": "f_carrier-a_update-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 5,
				"feature_code": "carrier",
				"feature_name": "carrier",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 24,
			"feature_id": 6,
			"action_code": "ed217d25bd8f",
			"action_name": "f_ccare-a_create-po",
			"action_desc": "f_ccare-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 6,
				"feature_code": "ccare",
				"feature_name": "ccare",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 25,
			"feature_id": 6,
			"action_code": "41ef8c542055",
			"action_name": "f_ccare-a_edit-ge",
			"action_desc": "f_ccare-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 6,
				"feature_code": "ccare",
				"feature_name": "ccare",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 26,
			"feature_id": 6,
			"action_code": "f6d2ca739b2b",
			"action_name": "f_ccare-a_index-ge",
			"action_desc": "f_ccare-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 6,
				"feature_code": "ccare",
				"feature_name": "ccare",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 27,
			"feature_id": 6,
			"action_code": "1f3e8aef9da7",
			"action_name": "f_ccare-a_new-ge",
			"action_desc": "f_ccare-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 6,
				"feature_code": "ccare",
				"feature_name": "ccare",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 28,
			"feature_id": 6,
			"action_code": "07d3c48a0504",
			"action_name": "f_ccare-a_patccare-ge",
			"action_desc": "f_ccare-a_patccare-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 6,
				"feature_code": "ccare",
				"feature_name": "ccare",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 29,
			"feature_id": 7,
			"action_code": "ecb2c743a4c3",
			"action_name": "f_ccaredef-a_edit-ge",
			"action_desc": "f_ccaredef-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 7,
				"feature_code": "ccaredef",
				"feature_name": "ccaredef",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 30,
			"feature_id": 7,
			"action_code": "759f1c570595",
			"action_name": "f_ccaredef-a_getccdef-ge",
			"action_desc": "f_ccaredef-a_getccdef-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 7,
				"feature_code": "ccaredef",
				"feature_name": "ccaredef",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 31,
			"feature_id": 7,
			"action_code": "e4318d2b18b8",
			"action_name": "f_ccaredef-a_index-ge",
			"action_desc": "f_ccaredef-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 7,
				"feature_code": "ccaredef",
				"feature_name": "ccaredef",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 32,
			"feature_id": 7,
			"action_code": "9b5cf5f78dc6",
			"action_name": "f_ccaredef-a_new-ge",
			"action_desc": "f_ccaredef-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 7,
				"feature_code": "ccaredef",
				"feature_name": "ccaredef",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 33,
			"feature_id": 7,
			"action_code": "6bf7584f197f",
			"action_name": "f_ccaredef-a_show-ge",
			"action_desc": "f_ccaredef-a_show-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 7,
				"feature_code": "ccaredef",
				"feature_name": "ccaredef",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 34,
			"feature_id": 8,
			"action_code": "52d7bf467da5",
			"action_name": "f_claim-a_claimsofpat-ge",
			"action_desc": "f_claim-a_claimsofpat-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 8,
				"feature_code": "claim",
				"feature_name": "claim",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 35,
			"feature_id": 8,
			"action_code": "1eb65eaede53",
			"action_name": "f_claim-a_getclmdetails-ge",
			"action_desc": "f_claim-a_getclmdetails-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 8,
				"feature_code": "claim",
				"feature_name": "claim",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 36,
			"feature_id": 9,
			"action_code": "009820cdaa39",
			"action_name": "f_commlogs-a_create-ge",
			"action_desc": "f_commlogs-a_create-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 9,
				"feature_code": "commlogs",
				"feature_name": "commlogs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 37,
			"feature_id": 9,
			"action_code": "c417b89a9477",
			"action_name": "f_commlogs-a_edit-ge",
			"action_desc": "f_commlogs-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 9,
				"feature_code": "commlogs",
				"feature_name": "commlogs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 38,
			"feature_id": 9,
			"action_code": "ed9dc5eaacf8",
			"action_name": "f_commlogs-a_index-ge",
			"action_desc": "f_commlogs-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 9,
				"feature_code": "commlogs",
				"feature_name": "commlogs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 39,
			"feature_id": 9,
			"action_code": "80a8c16fdce5",
			"action_name": "f_commlogs-a_new-ge",
			"action_desc": "f_commlogs-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 9,
				"feature_code": "commlogs",
				"feature_name": "commlogs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 40,
			"feature_id": 9,
			"action_code": "ad3f9bf45f8c",
			"action_name": "f_commlogs-a_show-ge",
			"action_desc": "f_commlogs-a_show-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 9,
				"feature_code": "commlogs",
				"feature_name": "commlogs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 41,
			"feature_id": 10,
			"action_code": "bb8528c520cb",
			"action_name": "f_dashboard-a_collections-ge",
			"action_desc": "f_dashboard-a_collections-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 10,
				"feature_code": "dashboard",
				"feature_name": "dashboard",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 42,
			"feature_id": 10,
			"action_code": "761e1ad7e3fb",
			"action_name": "f_dashboard-a_index-ge",
			"action_desc": "f_dashboard-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 10,
				"feature_code": "dashboard",
				"feature_name": "dashboard",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 43,
			"feature_id": 10,
			"action_code": "6118313293ca",
			"action_name": "f_dashboard-a_oldashboard-ge",
			"action_desc": "f_dashboard-a_oldashboard-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 10,
				"feature_code": "dashboard",
				"feature_name": "dashboard",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 44,
			"feature_id": 11,
			"action_code": "38d8222135d7",
			"action_name": "f_exam-a_create-ge",
			"action_desc": "f_exam-a_create-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 11,
				"feature_code": "exam",
				"feature_name": "exam",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 45,
			"feature_id": 11,
			"action_code": "25cbc05ee58f",
			"action_name": "f_exam-a_createdxtx-po",
			"action_desc": "f_exam-a_createdxtx-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 11,
				"feature_code": "exam",
				"feature_name": "exam",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 46,
			"feature_id": 11,
			"action_code": "3386ea36ab67",
			"action_name": "f_exam-a_edit-ge",
			"action_desc": "f_exam-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 11,
				"feature_code": "exam",
				"feature_name": "exam",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 47,
			"feature_id": 11,
			"action_code": "7121ed4e3ca1",
			"action_name": "f_exam-a_exammobi-ge",
			"action_desc": "f_exam-a_exammobi-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 11,
				"feature_code": "exam",
				"feature_name": "exam",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 48,
			"feature_id": 11,
			"action_code": "ac7c442581d0",
			"action_name": "f_exam-a_new-ge",
			"action_desc": "f_exam-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 11,
				"feature_code": "exam",
				"feature_name": "exam",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 49,
			"feature_id": 11,
			"action_code": "9e293dcafb94",
			"action_name": "f_exam-a_thexm-ge",
			"action_desc": "f_exam-a_thexm-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 11,
				"feature_code": "exam",
				"feature_name": "exam",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 50,
			"feature_id": 11,
			"action_code": "63cc6ee82428",
			"action_name": "f_exam-a_update-po",
			"action_desc": "f_exam-a_update-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 11,
				"feature_code": "exam",
				"feature_name": "exam",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 51,
			"feature_id": 12,
			"action_code": "d9d767c96b6f",
			"action_name": "f_feesch-a_create-po",
			"action_desc": "f_feesch-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 12,
				"feature_code": "feesch",
				"feature_name": "feesch",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 52,
			"feature_id": 12,
			"action_code": "dc00c2a67653",
			"action_name": "f_feesch-a_delete-ge",
			"action_desc": "f_feesch-a_delete-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 12,
				"feature_code": "feesch",
				"feature_name": "feesch",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 53,
			"feature_id": 12,
			"action_code": "e442e288d4db",
			"action_name": "f_feesch-a_edit-ge",
			"action_desc": "f_feesch-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 12,
				"feature_code": "feesch",
				"feature_name": "feesch",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 54,
			"feature_id": 12,
			"action_code": "8c6fe91af993",
			"action_name": "f_feesch-a_index-ge",
			"action_desc": "f_feesch-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 12,
				"feature_code": "feesch",
				"feature_name": "feesch",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 55,
			"feature_id": 12,
			"action_code": "2014ff2d738b",
			"action_name": "f_feesch-a_new-ge",
			"action_desc": "f_feesch-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 12,
				"feature_code": "feesch",
				"feature_name": "feesch",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 56,
			"feature_id": 12,
			"action_code": "b48fda6edb6b",
			"action_name": "f_feesch-a_update-po",
			"action_desc": "f_feesch-a_update-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 12,
				"feature_code": "feesch",
				"feature_name": "feesch",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 57,
			"feature_id": 12,
			"action_code": "0fdd6da54a6a",
			"action_name": "f_feesch-a_updfee-ge",
			"action_desc": "f_feesch-a_updfee-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 12,
				"feature_code": "feesch",
				"feature_name": "feesch",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 58,
			"feature_id": 12,
			"action_code": "a6787af11974",
			"action_name": "f_feesch-a_uploadet-po",
			"action_desc": "f_feesch-a_uploadet-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 12,
				"feature_code": "feesch",
				"feature_name": "feesch",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 59,
			"feature_id": 13,
			"action_code": "bb0591704f3b",
			"action_name": "f_goal-a_allopengoals-ge",
			"action_desc": "f_goal-a_allopengoals-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 13,
				"feature_code": "goal",
				"feature_name": "goal",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 60,
			"feature_id": 13,
			"action_code": "b44c5a3e784b",
			"action_name": "f_goal-a_create-ge",
			"action_desc": "f_goal-a_create-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 13,
				"feature_code": "goal",
				"feature_name": "goal",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 61,
			"feature_id": 13,
			"action_code": "3c9369b9fba4",
			"action_name": "f_goal-a_edit-ge",
			"action_desc": "f_goal-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 13,
				"feature_code": "goal",
				"feature_name": "goal",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 62,
			"feature_id": 13,
			"action_code": "129271b9a871",
			"action_name": "f_goal-a_index-ge",
			"action_desc": "f_goal-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 13,
				"feature_code": "goal",
				"feature_name": "goal",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 63,
			"feature_id": 13,
			"action_code": "fb91fc6db89a",
			"action_name": "f_goal-a_new-ge",
			"action_desc": "f_goal-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 13,
				"feature_code": "goal",
				"feature_name": "goal",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 64,
			"feature_id": 13,
			"action_code": "309f442e27a6",
			"action_name": "f_goal-a_patgoalcount-ge",
			"action_desc": "f_goal-a_patgoalcount-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 13,
				"feature_code": "goal",
				"feature_name": "goal",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 65,
			"feature_id": 13,
			"action_code": "4c855444305f",
			"action_name": "f_goal-a_update-ge",
			"action_desc": "f_goal-a_update-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 13,
				"feature_code": "goal",
				"feature_name": "goal",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 66,
			"feature_id": 13,
			"action_code": "415b87028673",
			"action_name": "f_goal-a_updstats-ge",
			"action_desc": "f_goal-a_updstats-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 13,
				"feature_code": "goal",
				"feature_name": "goal",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 67,
			"feature_id": 14,
			"action_code": "bc0214309f54",
			"action_name": "f_insplan-a_bendet-ge",
			"action_desc": "f_insplan-a_bendet-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 68,
			"feature_id": 14,
			"action_code": "ccaf3c3fd885",
			"action_name": "f_insplan-a_bensum-ge",
			"action_desc": "f_insplan-a_bensum-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 69,
			"feature_id": 14,
			"action_code": "d03eed4de5b9",
			"action_name": "f_insplan-a_create-po",
			"action_desc": "f_insplan-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 70,
			"feature_id": 14,
			"action_code": "335d87bae0b4",
			"action_name": "f_insplan-a_edit-ge",
			"action_desc": "f_insplan-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 71,
			"feature_id": 14,
			"action_code": "841d6acea0d8",
			"action_name": "f_insplan-a_getinscovcat-ge",
			"action_desc": "f_insplan-a_getinscovcat-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 72,
			"feature_id": 14,
			"action_code": "5853f262b180",
			"action_name": "f_insplan-a_index-ge",
			"action_desc": "f_insplan-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 73,
			"feature_id": 14,
			"action_code": "4e0698f79e8a",
			"action_name": "f_insplan-a_new-ge",
			"action_desc": "f_insplan-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 74,
			"feature_id": 14,
			"action_code": "d8c60e558510",
			"action_name": "f_insplan-a_update-pa",
			"action_desc": "f_insplan-a_update-pa",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 75,
			"feature_id": 14,
			"action_code": "30d46eeca4d8",
			"action_name": "f_insplan-a_updbensum-po",
			"action_desc": "f_insplan-a_updbensum-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 76,
			"feature_id": 14,
			"action_code": "9a5fc069d158",
			"action_name": "f_insplan-a_updbenval-po",
			"action_desc": "f_insplan-a_updbenval-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 14,
				"feature_code": "insplan",
				"feature_name": "insplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 77,
			"feature_id": 15,
			"action_code": "1bc7a918b044",
			"action_name": "f_invoices-a_adddetails-ge",
			"action_desc": "f_invoices-a_adddetails-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 78,
			"feature_id": 15,
			"action_code": "1aac0e5fc654",
			"action_name": "f_invoices-a_create-po",
			"action_desc": "f_invoices-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 79,
			"feature_id": 15,
			"action_code": "2435107e52a9",
			"action_name": "f_invoices-a_destroy-po",
			"action_desc": "f_invoices-a_destroy-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 80,
			"feature_id": 15,
			"action_code": "3bb23f152915",
			"action_name": "f_invoices-a_edit-ge",
			"action_desc": "f_invoices-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 81,
			"feature_id": 15,
			"action_code": "4ebf98f20936",
			"action_name": "f_invoices-a_editinv-ge",
			"action_desc": "f_invoices-a_editinv-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 82,
			"feature_id": 15,
			"action_code": "ea033b65e7cd",
			"action_name": "f_invoices-a_invalidate-po",
			"action_desc": "f_invoices-a_invalidate-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 83,
			"feature_id": 15,
			"action_code": "beada695c897",
			"action_name": "f_invoices-a_new-ge",
			"action_desc": "f_invoices-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 84,
			"feature_id": 15,
			"action_code": "cbd12cb496d9",
			"action_name": "f_invoices-a_patinvs-ge",
			"action_desc": "f_invoices-a_patinvs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 85,
			"feature_id": 15,
			"action_code": "f5f9c32d6acb",
			"action_name": "f_invoices-a_removedetails-ge",
			"action_desc": "f_invoices-a_removedetails-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 15,
				"feature_code": "invoices",
				"feature_name": "invoices",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 86,
			"feature_id": 16,
			"action_code": "ed6546d1b7b9",
			"action_name": "f_note-a_create-ge",
			"action_desc": "f_note-a_create-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 16,
				"feature_code": "note",
				"feature_name": "note",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 87,
			"feature_id": 16,
			"action_code": "d9a7beeac80f",
			"action_name": "f_note-a_tnotes-ge",
			"action_desc": "f_note-a_tnotes-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 16,
				"feature_code": "note",
				"feature_name": "note",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 88,
			"feature_id": 17,
			"action_code": "ab3e71377a5f",
			"action_name": "f_notification-a_notify-ge",
			"action_desc": "f_notification-a_notify-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 17,
				"feature_code": "notification",
				"feature_name": "notification",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 89,
			"feature_id": 18,
			"action_code": "11e4c7603aff",
			"action_name": "f_patient-a_addgoal-ge",
			"action_desc": "f_patient-a_addgoal-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 90,
			"feature_id": 18,
			"action_code": "6fcf2c5c87be",
			"action_name": "f_patient-a_angpatinfo-ge",
			"action_desc": "f_patient-a_angpatinfo-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 91,
			"feature_id": 18,
			"action_code": "cbe8ada31cd7",
			"action_name": "f_patient-a_create-po",
			"action_desc": "f_patient-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 92,
			"feature_id": 18,
			"action_code": "bae583271824",
			"action_name": "f_patient-a_edit-ge",
			"action_desc": "f_patient-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 93,
			"feature_id": 18,
			"action_code": "bafe3ab78310",
			"action_name": "f_patient-a_gettprocsofpat-ge",
			"action_desc": "f_patient-a_gettprocsofpat-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 94,
			"feature_id": 18,
			"action_code": "66abf4b389e9",
			"action_name": "f_patient-a_new-po",
			"action_desc": "f_patient-a_new-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 95,
			"feature_id": 18,
			"action_code": "84ef8d7712d2",
			"action_name": "f_patient-a_patcarrierdet-ge",
			"action_desc": "f_patient-a_patcarrierdet-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 96,
			"feature_id": 18,
			"action_code": "73d85f7908f0",
			"action_name": "f_patient-a_patcarriersummary-ge",
			"action_desc": "f_patient-a_patcarriersummary-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 97,
			"feature_id": 18,
			"action_code": "f93f9db8861b",
			"action_name": "f_patient-a_patcommlog-ge",
			"action_desc": "f_patient-a_patcommlog-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 98,
			"feature_id": 18,
			"action_code": "3c8d23227ab2",
			"action_name": "f_patient-a_patdet-ge",
			"action_desc": "f_patient-a_patdet-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 99,
			"feature_id": 18,
			"action_code": "9deda7049bb7",
			"action_name": "f_patient-a_patexam-ge",
			"action_desc": "f_patient-a_patexam-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 100,
			"feature_id": 18,
			"action_code": "78f2b1f267a5",
			"action_name": "f_patient-a_patfamily-ge",
			"action_desc": "f_patient-a_patfamily-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 101,
			"feature_id": 18,
			"action_code": "2ee3f3621268",
			"action_name": "f_patient-a_patgoals-ge",
			"action_desc": "f_patient-a_patgoals-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 102,
			"feature_id": 18,
			"action_code": "ce87e209b47c",
			"action_name": "f_patient-a_patinfo-ge",
			"action_desc": "f_patient-a_patinfo-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 103,
			"feature_id": 18,
			"action_code": "0766b153fc40",
			"action_name": "f_patient-a_patsearch-ge",
			"action_desc": "f_patient-a_patsearch-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 104,
			"feature_id": 18,
			"action_code": "3d10a697d40b",
			"action_name": "f_patient-a_recarelist-ge",
			"action_desc": "f_patient-a_recarelist-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 105,
			"feature_id": 18,
			"action_code": "6d749669a476",
			"action_name": "f_patient-a_update-po",
			"action_desc": "f_patient-a_update-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 106,
			"feature_id": 18,
			"action_code": "66502e11d984",
			"action_name": "f_patient-a_updaterm-po",
			"action_desc": "f_patient-a_updaterm-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 107,
			"feature_id": 18,
			"action_code": "ab4595c333ec",
			"action_name": "f_patient-a_updpatar-ge",
			"action_desc": "f_patient-a_updpatar-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 108,
			"feature_id": 18,
			"action_code": "e1266b44f6f3",
			"action_name": "f_patient-a_updpatbs-ge",
			"action_desc": "f_patient-a_updpatbs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 109,
			"feature_id": 18,
			"action_code": "9e412dfe0727",
			"action_name": "f_patient-a_updpathyg-ge",
			"action_desc": "f_patient-a_updpathyg-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 110,
			"feature_id": 18,
			"action_code": "67daa67896b0",
			"action_name": "f_patient-a_updpatprov-ge",
			"action_desc": "f_patient-a_updpatprov-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 111,
			"feature_id": 18,
			"action_code": "89d6c73ce99b",
			"action_name": "f_patient-a_updpatrm-ge",
			"action_desc": "f_patient-a_updpatrm-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 18,
				"feature_code": "patient",
				"feature_name": "patient",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 112,
			"feature_id": 19,
			"action_code": "4be5b442a7de",
			"action_name": "f_patpayments-a_appy-po",
			"action_desc": "f_patpayments-a_appy-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 19,
				"feature_code": "patpayments",
				"feature_name": "patpayments",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 113,
			"feature_id": 19,
			"action_code": "b8db5252e69a",
			"action_name": "f_patpayments-a_create-po",
			"action_desc": "f_patpayments-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 19,
				"feature_code": "patpayments",
				"feature_name": "patpayments",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 114,
			"feature_id": 19,
			"action_code": "ca49de94f016",
			"action_name": "f_patpayments-a_edit-ge",
			"action_desc": "f_patpayments-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 19,
				"feature_code": "patpayments",
				"feature_name": "patpayments",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 115,
			"feature_id": 19,
			"action_code": "4fb101df9c48",
			"action_name": "f_patpayments-a_editpatpay-ge",
			"action_desc": "f_patpayments-a_editpatpay-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 19,
				"feature_code": "patpayments",
				"feature_name": "patpayments",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 116,
			"feature_id": 19,
			"action_code": "3f5724599ba7",
			"action_name": "f_patpayments-a_new-ge",
			"action_desc": "f_patpayments-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 19,
				"feature_code": "patpayments",
				"feature_name": "patpayments",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 117,
			"feature_id": 19,
			"action_code": "5f60113dd6f5",
			"action_name": "f_patpayments-a_patbalpmts-ge",
			"action_desc": "f_patpayments-a_patbalpmts-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 19,
				"feature_code": "patpayments",
				"feature_name": "patpayments",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 118,
			"feature_id": 19,
			"action_code": "89a186df20a2",
			"action_name": "f_patpayments-a_patpays-ge",
			"action_desc": "f_patpayments-a_patpays-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 19,
				"feature_code": "patpayments",
				"feature_name": "patpayments",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 119,
			"feature_id": 20,
			"action_code": "4d94bab287cc",
			"action_name": "f_pattooth-a_addtooth-ge",
			"action_desc": "f_pattooth-a_addtooth-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 20,
				"feature_code": "pattooth",
				"feature_name": "pattooth",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 120,
			"feature_id": 20,
			"action_code": "03f276267f24",
			"action_name": "f_pattooth-a_markexist-po",
			"action_desc": "f_pattooth-a_markexist-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 20,
				"feature_code": "pattooth",
				"feature_name": "pattooth",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 121,
			"feature_id": 20,
			"action_code": "0c17e56ec2b9",
			"action_name": "f_pattooth-a_markmissing-po",
			"action_desc": "f_pattooth-a_markmissing-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 20,
				"feature_code": "pattooth",
				"feature_name": "pattooth",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 122,
			"feature_id": 20,
			"action_code": "18e7bb5f40d1",
			"action_name": "f_pattooth-a_remainingtooth-ge",
			"action_desc": "f_pattooth-a_remainingtooth-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 20,
				"feature_code": "pattooth",
				"feature_name": "pattooth",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 123,
			"feature_id": 20,
			"action_code": "a0eb34059310",
			"action_name": "f_pattooth-a_removeth-po",
			"action_desc": "f_pattooth-a_removeth-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 20,
				"feature_code": "pattooth",
				"feature_name": "pattooth",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 124,
			"feature_id": 21,
			"action_code": "c45fce9b1ad9",
			"action_name": "f_proccode-a_codeofcat-ge",
			"action_desc": "f_proccode-a_codeofcat-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 21,
				"feature_code": "proccode",
				"feature_name": "proccode",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 125,
			"feature_id": 21,
			"action_code": "fcb0dea6941e",
			"action_name": "f_proccode-a_codesoftx-ge",
			"action_desc": "f_proccode-a_codesoftx-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 21,
				"feature_code": "proccode",
				"feature_name": "proccode",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 126,
			"feature_id": 21,
			"action_code": "a26a6ab24d84",
			"action_name": "f_proccode-a_edit-ge",
			"action_desc": "f_proccode-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 21,
				"feature_code": "proccode",
				"feature_name": "proccode",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 127,
			"feature_id": 21,
			"action_code": "b5c260f889da",
			"action_name": "f_proccode-a_getcodedetails-ge",
			"action_desc": "f_proccode-a_getcodedetails-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 21,
				"feature_code": "proccode",
				"feature_name": "proccode",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 128,
			"feature_id": 21,
			"action_code": "97add18b0421",
			"action_name": "f_proccode-a_getcodes-ge",
			"action_desc": "f_proccode-a_getcodes-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 21,
				"feature_code": "proccode",
				"feature_name": "proccode",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 129,
			"feature_id": 21,
			"action_code": "7c2d16da8d16",
			"action_name": "f_proccode-a_index-ge",
			"action_desc": "f_proccode-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 21,
				"feature_code": "proccode",
				"feature_name": "proccode",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 130,
			"feature_id": 21,
			"action_code": "5f59e7a0794f",
			"action_name": "f_proccode-a_proccodeinsfee-ge",
			"action_desc": "f_proccode-a_proccodeinsfee-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 21,
				"feature_code": "proccode",
				"feature_name": "proccode",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 131,
			"feature_id": 22,
			"action_code": "cb1e7f0a4b74",
			"action_name": "f_providers-a_edit-ge",
			"action_desc": "f_providers-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 22,
				"feature_code": "providers",
				"feature_name": "providers",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 132,
			"feature_id": 22,
			"action_code": "3c5f6551a061",
			"action_name": "f_providers-a_index-ge",
			"action_desc": "f_providers-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 22,
				"feature_code": "providers",
				"feature_name": "providers",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 133,
			"feature_id": 22,
			"action_code": "f8efe9796491",
			"action_name": "f_providers-a_new-ge",
			"action_desc": "f_providers-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 22,
				"feature_code": "providers",
				"feature_name": "providers",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 134,
			"feature_id": 22,
			"action_code": "4f1a849138d8",
			"action_name": "f_providers-a_show-ge",
			"action_desc": "f_providers-a_show-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 22,
				"feature_code": "providers",
				"feature_name": "providers",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 135,
			"feature_id": 23,
			"action_code": "5938f2c983b7",
			"action_name": "f_quote-a_adddetails-ge",
			"action_desc": "f_quote-a_adddetails-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 136,
			"feature_id": 23,
			"action_code": "ad17f022f4e3",
			"action_name": "f_quote-a_approvequote-ge",
			"action_desc": "f_quote-a_approvequote-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 137,
			"feature_id": 23,
			"action_code": "ea78c70456f0",
			"action_name": "f_quote-a_calculate-ge",
			"action_desc": "f_quote-a_calculate-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 138,
			"feature_id": 23,
			"action_code": "3d86078cda98",
			"action_name": "f_quote-a_cancelqt-ge",
			"action_desc": "f_quote-a_cancelqt-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 139,
			"feature_id": 23,
			"action_code": "d4b55ab4a8d8",
			"action_name": "f_quote-a_cg-ge",
			"action_desc": "f_quote-a_cg-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 140,
			"feature_id": 23,
			"action_code": "674fbc9b31f2",
			"action_name": "f_quote-a_chgqd-po",
			"action_desc": "f_quote-a_chgqd-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 141,
			"feature_id": 23,
			"action_code": "e20dd831b86a",
			"action_name": "f_quote-a_create-po",
			"action_desc": "f_quote-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 142,
			"feature_id": 23,
			"action_code": "15b8cd269c7a",
			"action_name": "f_quote-a_createquotes-po",
			"action_desc": "f_quote-a_createquotes-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 143,
			"feature_id": 23,
			"action_code": "d51952c81f76",
			"action_name": "f_quote-a_downloadpdf-po",
			"action_desc": "f_quote-a_downloadpdf-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 144,
			"feature_id": 23,
			"action_code": "da6efe924807",
			"action_name": "f_quote-a_edit-ge",
			"action_desc": "f_quote-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 145,
			"feature_id": 23,
			"action_code": "75b85ccdf9dc",
			"action_name": "f_quote-a_index-ge",
			"action_desc": "f_quote-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 146,
			"feature_id": 23,
			"action_code": "9f4cfae24dac",
			"action_name": "f_quote-a_printqt-ge",
			"action_desc": "f_quote-a_printqt-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 147,
			"feature_id": 23,
			"action_code": "65c9724dbec3",
			"action_name": "f_quote-a_quoteopts-ge",
			"action_desc": "f_quote-a_quoteopts-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 148,
			"feature_id": 23,
			"action_code": "93f1df09d07c",
			"action_name": "f_quote-a_removedetails-ge",
			"action_desc": "f_quote-a_removedetails-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 149,
			"feature_id": 23,
			"action_code": "2b0f10c70eb5",
			"action_name": "f_quote-a_reorder-ge",
			"action_desc": "f_quote-a_reorder-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 150,
			"feature_id": 23,
			"action_code": "828f0b27a063",
			"action_name": "f_quote-a_update-po",
			"action_desc": "f_quote-a_update-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 23,
				"feature_code": "quote",
				"feature_name": "quote",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 151,
			"feature_id": 24,
			"action_code": "ed974fc66973",
			"action_name": "f_reports-a_agingar-ge",
			"action_desc": "f_reports-a_agingar-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 152,
			"feature_id": 24,
			"action_code": "1c7b3e5d0da2",
			"action_name": "f_reports-a_agingreport-ge",
			"action_desc": "f_reports-a_agingreport-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 153,
			"feature_id": 24,
			"action_code": "c3d1e954d689",
			"action_name": "f_reports-a_allprocs-ge",
			"action_desc": "f_reports-a_allprocs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 154,
			"feature_id": 24,
			"action_code": "89762d2eb6f0",
			"action_name": "f_reports-a_aptdetail-ge",
			"action_desc": "f_reports-a_aptdetail-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 155,
			"feature_id": 24,
			"action_code": "07d85c9a59e3",
			"action_name": "f_reports-a_aptlistsummary-ge",
			"action_desc": "f_reports-a_aptlistsummary-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 156,
			"feature_id": 24,
			"action_code": "9a514e0cd1e9",
			"action_name": "f_reports-a_aptlistsummarya-ge",
			"action_desc": "f_reports-a_aptlistsummarya-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 157,
			"feature_id": 24,
			"action_code": "42d1a033952b",
			"action_name": "f_reports-a_aptrep-ge",
			"action_desc": "f_reports-a_aptrep-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 158,
			"feature_id": 24,
			"action_code": "a4eba0b77942",
			"action_name": "f_reports-a_aptsummary-ge",
			"action_desc": "f_reports-a_aptsummary-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 159,
			"feature_id": 24,
			"action_code": "d311726cd540",
			"action_name": "f_reports-a_carrierlist-ge",
			"action_desc": "f_reports-a_carrierlist-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 160,
			"feature_id": 24,
			"action_code": "9a3348752d66",
			"action_name": "f_reports-a_ccarepats-ge",
			"action_desc": "f_reports-a_ccarepats-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 161,
			"feature_id": 24,
			"action_code": "710832043b04",
			"action_name": "f_reports-a_claimreport-ge",
			"action_desc": "f_reports-a_claimreport-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 162,
			"feature_id": 24,
			"action_code": "3fdf2c64bbec",
			"action_name": "f_reports-a_collectionrept-ge",
			"action_desc": "f_reports-a_collectionrept-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 163,
			"feature_id": 24,
			"action_code": "f7ad24f6f950",
			"action_name": "f_reports-a_collrpt-ge",
			"action_desc": "f_reports-a_collrpt-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 164,
			"feature_id": 24,
			"action_code": "a0acb46c933c",
			"action_name": "f_reports-a_completedprocedures-ge",
			"action_desc": "f_reports-a_completedprocedures-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 165,
			"feature_id": 24,
			"action_code": "2bbbf9d8a6e0",
			"action_name": "f_reports-a_editsubnote-ge",
			"action_desc": "f_reports-a_editsubnote-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 166,
			"feature_id": 24,
			"action_code": "54712c8520be",
			"action_name": "f_reports-a_feeschedulelist-ge",
			"action_desc": "f_reports-a_feeschedulelist-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 167,
			"feature_id": 24,
			"action_code": "bec79209072b",
			"action_name": "f_reports-a_index-ge",
			"action_desc": "f_reports-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 168,
			"feature_id": 24,
			"action_code": "3c0fea1a6443",
			"action_name": "f_reports-a_insplanlist-ge",
			"action_desc": "f_reports-a_insplanlist-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 169,
			"feature_id": 24,
			"action_code": "c697c9703bea",
			"action_name": "f_reports-a_newpatreport-ge",
			"action_desc": "f_reports-a_newpatreport-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 170,
			"feature_id": 24,
			"action_code": "0ceb81ce5817",
			"action_name": "f_reports-a_notifyby-po",
			"action_desc": "f_reports-a_notifyby-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 171,
			"feature_id": 24,
			"action_code": "1e4fa8a2df60",
			"action_name": "f_reports-a_onlyrecallpatients-ge",
			"action_desc": "f_reports-a_onlyrecallpatients-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 172,
			"feature_id": 24,
			"action_code": "5bf74a0ec3b1",
			"action_name": "f_reports-a_patinfo-ge",
			"action_desc": "f_reports-a_patinfo-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 173,
			"feature_id": 24,
			"action_code": "3ea31f63f299",
			"action_name": "f_reports-a_proccodelist-ge",
			"action_desc": "f_reports-a_proccodelist-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 174,
			"feature_id": 24,
			"action_code": "eaacae0899ef",
			"action_name": "f_reports-a_proddetailreport-ge",
			"action_desc": "f_reports-a_proddetailreport-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 175,
			"feature_id": 24,
			"action_code": "7a0ee0992f8f",
			"action_name": "f_reports-a_prodsummaryreport-ge",
			"action_desc": "f_reports-a_prodsummaryreport-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 176,
			"feature_id": 24,
			"action_code": "960d67c58415",
			"action_name": "f_reports-a_prodsupersummary-ge",
			"action_desc": "f_reports-a_prodsupersummary-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 177,
			"feature_id": 24,
			"action_code": "e1a8c0c7f054",
			"action_name": "f_reports-a_providerlist-ge",
			"action_desc": "f_reports-a_providerlist-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 178,
			"feature_id": 24,
			"action_code": "49d3f093aa4d",
			"action_name": "f_reports-a_recallapt-ge",
			"action_desc": "f_reports-a_recallapt-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 179,
			"feature_id": 24,
			"action_code": "3778e2e96188",
			"action_name": "f_reports-a_rvulist-ge",
			"action_desc": "f_reports-a_rvulist-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 180,
			"feature_id": 24,
			"action_code": "821df5c2144d",
			"action_name": "f_reports-a_rvureport-ge",
			"action_desc": "f_reports-a_rvureport-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 181,
			"feature_id": 24,
			"action_code": "034b4f9dc895",
			"action_name": "f_reports-a_unqpatreport-ge",
			"action_desc": "f_reports-a_unqpatreport-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 182,
			"feature_id": 24,
			"action_code": "6e07c4db08d4",
			"action_name": "f_reports-a_updaterm-po",
			"action_desc": "f_reports-a_updaterm-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 183,
			"feature_id": 24,
			"action_code": "468b61cc2333",
			"action_name": "f_reports-a_updsubnote-ge",
			"action_desc": "f_reports-a_updsubnote-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 24,
				"feature_code": "reports",
				"feature_name": "reports",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 184,
			"feature_id": 25,
			"action_code": "24941bf51606",
			"action_name": "f_session-a_create-po",
			"action_desc": "f_session-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 25,
				"feature_code": "session",
				"feature_name": "session",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 185,
			"feature_id": 25,
			"action_code": "410b098f0912",
			"action_name": "f_session-a_logout-ge",
			"action_desc": "f_session-a_logout-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 25,
				"feature_code": "session",
				"feature_name": "session",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 186,
			"feature_id": 25,
			"action_code": "9bd34afb4f98",
			"action_name": "f_session-a_new-ge",
			"action_desc": "f_session-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 25,
				"feature_code": "session",
				"feature_name": "session",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 187,
			"feature_id": 26,
			"action_code": "da3e0e358869",
			"action_name": "f_sessions-a_destroy-de",
			"action_desc": "f_sessions-a_destroy-de",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 26,
				"feature_code": "sessions",
				"feature_name": "sessions",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 188,
			"feature_id": 26,
			"action_code": "5ca31d027d3c",
			"action_name": "f_sessions-a_index-ge",
			"action_desc": "f_sessions-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 26,
				"feature_code": "sessions",
				"feature_name": "sessions",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 189,
			"feature_id": 26,
			"action_code": "85eee2d25ca8",
			"action_name": "f_sessions-a_new-ge",
			"action_desc": "f_sessions-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 26,
				"feature_code": "sessions",
				"feature_name": "sessions",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 190,
			"feature_id": 27,
			"action_code": "94fcccb51e28",
			"action_name": "f_sheetmaster-a_create-ge",
			"action_desc": "f_sheetmaster-a_create-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 27,
				"feature_code": "sheetmaster",
				"feature_name": "sheetmaster",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 191,
			"feature_id": 27,
			"action_code": "262688a9c49d",
			"action_name": "f_sheetmaster-a_edit-ge",
			"action_desc": "f_sheetmaster-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 27,
				"feature_code": "sheetmaster",
				"feature_name": "sheetmaster",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 192,
			"feature_id": 27,
			"action_code": "e88d741a756f",
			"action_name": "f_sheetmaster-a_index-ge",
			"action_desc": "f_sheetmaster-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 27,
				"feature_code": "sheetmaster",
				"feature_name": "sheetmaster",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 193,
			"feature_id": 27,
			"action_code": "92e626d41a53",
			"action_name": "f_sheetmaster-a_new-ge",
			"action_desc": "f_sheetmaster-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 27,
				"feature_code": "sheetmaster",
				"feature_name": "sheetmaster",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 194,
			"feature_id": 27,
			"action_code": "d8d54e9c3f80",
			"action_name": "f_sheetmaster-a_remove-ge",
			"action_desc": "f_sheetmaster-a_remove-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 27,
				"feature_code": "sheetmaster",
				"feature_name": "sheetmaster",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 195,
			"feature_id": 27,
			"action_code": "aa753f8f5b8f",
			"action_name": "f_sheetmaster-a_update-ge",
			"action_desc": "f_sheetmaster-a_update-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 27,
				"feature_code": "sheetmaster",
				"feature_name": "sheetmaster",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 196,
			"feature_id": 28,
			"action_code": "89f412cbd723",
			"action_name": "f_staffs-a_cpwd-po",
			"action_desc": "f_staffs-a_cpwd-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 28,
				"feature_code": "staffs",
				"feature_name": "staffs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 197,
			"feature_id": 28,
			"action_code": "a4bf03c5ec14",
			"action_name": "f_staffs-a_edit-ge",
			"action_desc": "f_staffs-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 28,
				"feature_code": "staffs",
				"feature_name": "staffs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 198,
			"feature_id": 28,
			"action_code": "9dd64cced133",
			"action_name": "f_staffs-a_ep-ge",
			"action_desc": "f_staffs-a_ep-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 28,
				"feature_code": "staffs",
				"feature_name": "staffs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 199,
			"feature_id": 28,
			"action_code": "ce6604078593",
			"action_name": "f_staffs-a_index-ge",
			"action_desc": "f_staffs-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 28,
				"feature_code": "staffs",
				"feature_name": "staffs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 200,
			"feature_id": 28,
			"action_code": "996083f1143e",
			"action_name": "f_staffs-a_new-ge",
			"action_desc": "f_staffs-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 28,
				"feature_code": "staffs",
				"feature_name": "staffs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 201,
			"feature_id": 28,
			"action_code": "742bd7dc98e9",
			"action_name": "f_staffs-a_rp-po",
			"action_desc": "f_staffs-a_rp-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 28,
				"feature_code": "staffs",
				"feature_name": "staffs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 202,
			"feature_id": 28,
			"action_code": "3a512bacfd3b",
			"action_name": "f_staffs-a_show-ge",
			"action_desc": "f_staffs-a_show-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 28,
				"feature_code": "staffs",
				"feature_name": "staffs",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 203,
			"feature_id": 29,
			"action_code": "b37bdb2c976f",
			"action_name": "f_subscriber-a_adddep-po",
			"action_desc": "f_subscriber-a_adddep-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 29,
				"feature_code": "subscriber",
				"feature_name": "subscriber",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 204,
			"feature_id": 29,
			"action_code": "a042830e22f6",
			"action_name": "f_subscriber-a_cp-po",
			"action_desc": "f_subscriber-a_cp-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 29,
				"feature_code": "subscriber",
				"feature_name": "subscriber",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 205,
			"feature_id": 29,
			"action_code": "ee51f86aea6d",
			"action_name": "f_subscriber-a_create-po",
			"action_desc": "f_subscriber-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 29,
				"feature_code": "subscriber",
				"feature_name": "subscriber",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 206,
			"feature_id": 29,
			"action_code": "e1928e512ec5",
			"action_name": "f_subscriber-a_index-ge",
			"action_desc": "f_subscriber-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 29,
				"feature_code": "subscriber",
				"feature_name": "subscriber",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 207,
			"feature_id": 29,
			"action_code": "fae803f59038",
			"action_name": "f_subscriber-a_new-po",
			"action_desc": "f_subscriber-a_new-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 29,
				"feature_code": "subscriber",
				"feature_name": "subscriber",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 208,
			"feature_id": 30,
			"action_code": "1b3ce7f9994a",
			"action_name": "f_task-a_create-ge",
			"action_desc": "f_task-a_create-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 209,
			"feature_id": 30,
			"action_code": "1af894886ade",
			"action_name": "f_task-a_createtask-ge",
			"action_desc": "f_task-a_createtask-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 210,
			"feature_id": 30,
			"action_code": "de765f527db2",
			"action_name": "f_task-a_edit-ge",
			"action_desc": "f_task-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 211,
			"feature_id": 30,
			"action_code": "0b4e6b058f64",
			"action_name": "f_task-a_getgoalactivities-ge",
			"action_desc": "f_task-a_getgoalactivities-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 212,
			"feature_id": 30,
			"action_code": "24852bb9cc20",
			"action_name": "f_task-a_index-ge",
			"action_desc": "f_task-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 213,
			"feature_id": 30,
			"action_code": "a3f9dc943e04",
			"action_name": "f_task-a_new-ge",
			"action_desc": "f_task-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 214,
			"feature_id": 30,
			"action_code": "092b181435d4",
			"action_name": "f_task-a_pattasks-ge",
			"action_desc": "f_task-a_pattasks-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 215,
			"feature_id": 30,
			"action_code": "69431c1da3e6",
			"action_name": "f_task-a_setcomplete-ge",
			"action_desc": "f_task-a_setcomplete-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 216,
			"feature_id": 30,
			"action_code": "0c638252218f",
			"action_name": "f_task-a_update-po",
			"action_desc": "f_task-a_update-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 217,
			"feature_id": 30,
			"action_code": "6868cbcc01f1",
			"action_name": "f_task-a_updstats-ge",
			"action_desc": "f_task-a_updstats-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 30,
				"feature_code": "task",
				"feature_name": "task",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 218,
			"feature_id": 31,
			"action_code": "9e04b308bc9d",
			"action_name": "f_tplan-a_new-ge",
			"action_desc": "f_tplan-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 31,
				"feature_code": "tplan",
				"feature_name": "tplan",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 219,
			"feature_id": 32,
			"action_code": "61504f00bbca",
			"action_name": "f_tproc-a_addfromopts-po",
			"action_desc": "f_tproc-a_addfromopts-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 220,
			"feature_id": 32,
			"action_code": "b8968c56b0d0",
			"action_name": "f_tproc-a_addprocs-ge",
			"action_desc": "f_tproc-a_addprocs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 221,
			"feature_id": 32,
			"action_code": "67fabd91b23b",
			"action_name": "f_tproc-a_baltprocs-ge",
			"action_desc": "f_tproc-a_baltprocs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 222,
			"feature_id": 32,
			"action_code": "c827f70d3d4c",
			"action_name": "f_tproc-a_chgproc-ge",
			"action_desc": "f_tproc-a_chgproc-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 223,
			"feature_id": 32,
			"action_code": "8ffd1acb047d",
			"action_name": "f_tproc-a_deltx-ge",
			"action_desc": "f_tproc-a_deltx-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 224,
			"feature_id": 32,
			"action_code": "3d370e39a171",
			"action_name": "f_tproc-a_getesheettotals-ge",
			"action_desc": "f_tproc-a_getesheettotals-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 225,
			"feature_id": 32,
			"action_code": "18ef07ca8f54",
			"action_name": "f_tproc-a_getprocsoftx-ge",
			"action_desc": "f_tproc-a_getprocsoftx-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 226,
			"feature_id": 32,
			"action_code": "baa2b21862f5",
			"action_name": "f_tproc-a_getprocsoftxone-ge",
			"action_desc": "f_tproc-a_getprocsoftxone-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 227,
			"feature_id": 32,
			"action_code": "b254250f82ce",
			"action_name": "f_tproc-a_getproposedtprocs-ge",
			"action_desc": "f_tproc-a_getproposedtprocs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 228,
			"feature_id": 32,
			"action_code": "3fa1708a89ab",
			"action_name": "f_tproc-a_gettprocsofpat-ge",
			"action_desc": "f_tproc-a_gettprocsofpat-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 229,
			"feature_id": 32,
			"action_code": "4814a18c984b",
			"action_name": "f_tproc-a_moveable-ge",
			"action_desc": "f_tproc-a_moveable-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 230,
			"feature_id": 32,
			"action_code": "38789928ba75",
			"action_name": "f_tproc-a_patprocs-ge",
			"action_desc": "f_tproc-a_patprocs-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 231,
			"feature_id": 32,
			"action_code": "7331f0ac9bb7",
			"action_name": "f_tproc-a_txdet-po",
			"action_desc": "f_tproc-a_txdet-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 232,
			"feature_id": 32,
			"action_code": "eadd56ea4004",
			"action_name": "f_tproc-a_unschtprocs-po",
			"action_desc": "f_tproc-a_unschtprocs-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 32,
				"feature_code": "tproc",
				"feature_name": "tproc",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 233,
			"feature_id": 33,
			"action_code": "5d475b8b56aa",
			"action_name": "f_user-a_create-po",
			"action_desc": "f_user-a_create-po",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 33,
				"feature_code": "user",
				"feature_name": "user",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 234,
			"feature_id": 33,
			"action_code": "af9037957e79",
			"action_name": "f_user-a_new-ge",
			"action_desc": "f_user-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 33,
				"feature_code": "user",
				"feature_name": "user",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 235,
			"feature_id": 34,
			"action_code": "364038b607fb",
			"action_name": "f_userauts-a_edit-ge",
			"action_desc": "f_userauts-a_edit-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 34,
				"feature_code": "userauts",
				"feature_name": "userauts",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 236,
			"feature_id": 34,
			"action_code": "ef1361b6ff08",
			"action_name": "f_userauts-a_index-ge",
			"action_desc": "f_userauts-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 34,
				"feature_code": "userauts",
				"feature_name": "userauts",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 237,
			"feature_id": 34,
			"action_code": "d90b80fd2613",
			"action_name": "f_userauts-a_new-ge",
			"action_desc": "f_userauts-a_new-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 34,
				"feature_code": "userauts",
				"feature_name": "userauts",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 238,
			"feature_id": 34,
			"action_code": "5f6f8d90b35d",
			"action_name": "f_userauts-a_show-ge",
			"action_desc": "f_userauts-a_show-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 34,
				"feature_code": "userauts",
				"feature_name": "userauts",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 239,
			"feature_id": 35,
			"action_code": "b065253dcdf6",
			"action_name": "f_workspace-a_index-ge",
			"action_desc": "f_workspace-a_index-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 35,
				"feature_code": "workspace",
				"feature_name": "workspace",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	},
	{
		"action": {
			"action_id": 240,
			"feature_id": 35,
			"action_code": "a7db52d7978e",
			"action_name": "f_workspace-a_totpat-ge",
			"action_desc": "f_workspace-a_totpat-ge",
			"status": 1,
			"add_by": 0,
			"add_on": "2030-04-19T01:20:00.000Z",
			"edit_by": 0,
			"edit_on": "2030-04-19T01:20:00.000Z",
			"feature": {
				"feature_id": 35,
				"feature_code": "workspace",
				"feature_name": "workspace",
				"feature_desc": "TBD",
				"status": 1,
				"add_by": 0,
				"add_on": "2030-04-19T01:20:00.000Z",
				"edit_by": 0,
				"edit_on": "2030-04-19T01:20:00.000Z"
			}
		}
	}
]
```
