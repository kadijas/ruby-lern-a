# Local environment notes

## 2019-May-06 19:27:38
- nested attributes and nested fields
	- https://api.rubyonrails.org/v4.1.6/classes/ActiveRecord/NestedAttributes/ClassMethods.html
	- https://guides.rubyonrails.org/v4.1.6/form_helpers.html#nested-forms
		- Don't forget to update the whitelisted params in your controller to also include the _destroy field:
	- https://apidock.com/rails/ActiveRecord/NestedAttributes/ClassMethods
- Save multiple Models with Form Objects and Transactions
	- https://revs.runtime-revolution.com/creating-form-objects-with-activemodel-346e6c2abbf3
		- allows a Ruby object to quack like an ActiveRecord model without being backed by a database table.
	- https://revs.runtime-revolution.com/saving-multiple-models-with-form-objects-and-transactions-2c26f37f7b9a

```
see 'nested attributes and nested fields'
	- accepts_nested_attributes_for (in model)


class Foo
  belongs_to  :parent,   :class_name => "Foo"
  has_many    :children, :class_name => "Foo", :foreign_key=> "parent_id"
end

Foo.transaction do
  a.save!
  b.save!
end

bars = Bar.find_all_by_some_attribute(:a)
foo = Foo.create
values = bars.map {|bar| "(#{foo.id},#{bar.id})"}.join(",")
connection.execute("INSERT INTO foos_bars (foo_id, bar_id) VALUES
#{values}")

https://gist.github.com/jackrg/76ade1724bd816292e4e

<%= f.text_field :matter, placeholder: "Matter", rows: "4" %>

Users
class User < ApplicationRecord
  has_many :deals
end

Deals
class Deal < ApplicationRecord
  belongs_to :user
  has_many :clients, inverse_of: :deal
  validates :headline, presence: true
  accepts_nested_attributes_for :clients, allow_destroy: true
end

Clients
class Client < ApplicationRecord
  belongs_to :deal, inverse_of: :clients
  validates :name, presence: true
  validates :deal_id, presence: true
end

https://stackoverflow.com/questions/17454886/ruby-on-rails-creating-multiple-children-in-one-parent-form

```


## 2019-05-06 12:42:41
```html.erb
# works
<%= form_for @feature, :url => { :action => "update" }, :html => { :method => :update } do |vA| %>

# works
<%= select_tag(:status, options_for_select(@StatusArray, @feature.status)) %>
<%= vA.select(:status,@StatusArray) %>

# for action id
puts "#{params[:controller]} + #{params["action"]} + #{request.method} >> #{params[:_method]}"
```

## Bundler
- install to local folder
  - should avoid download each time.
```sh
bundle install --path vendor/bundle

--path: Specify a different path than the system default ($BUNDLE_PATH or $GEM_HOME).
	Bundler will remember this value for future installs on this machine
```

## Commands Used
- With modified generators
- 2019-05-07 13:06:47
	```sh
	rails generate model RoleAuthTemplate
	Configuring Generators now...
	# Running via Spring preloader in process 1914
	#       invoke  active_record
	#       create    db/migrate/20190507073744_create_role_auth_templates.rb
	#       create    app/models/role_auth_template.rb
	#       invoke    test_unit
	#       create      test/models/role_auth_template_test.rb
	#       create      test/fixtures/role_auth_templates.yml

	rails generate controller roleauthtemplates
	# Running via Spring preloader in process 4131
    #   create  app/controllers/roleauthtemplates_controller.rb
    #   invoke  erb
    #   create    app/views/roleauthtemplates
    #   invoke  test_unit
    #   create    test/controllers/roleauthtemplates_controller_test.rb
    #   invoke  assets
    #   invoke    js
    #   create      app/assets/javascripts/roleauthtemplates.js
    #   invoke    scss

	# Botched, by using singular
	# Running via Spring preloader in process 1981
	#       create  app/controllers/roleauthtemplate_controller.rb
	#       invoke  erb
	#       create    app/views/roleauthtemplate
	#       invoke  test_unit
	#       create    test/controllers/roleauthtemplate_controller_test.rb
	#       invoke  assets
	#       invoke    js
	#       create      app/assets/javascripts/roleauthtemplate.js
	#       invoke    scss
	```
- Older
	```sh
	rails generate controller WfOne
	# Configuring Generators now...
	# Running via Spring preloader in process 8056
	#       create  app/controllers/wf_one_controller.rb
	#       invoke  erb
	#       create    app/views/wf_one
	#       invoke  test_unit
	#       create    test/controllers/wf_one_controller_test.rb
	#       invoke  assets
	#       invoke    js
	#       create      app/assets/javascripts/wf_one.js
	#       invoke    scss

	```
  	- Settings applied
		```rb
		# Settings in config/application.rb
			puts "Configuring Generators now..."
			config.generators do |g|
			g.javascript_engine :js
			g.helper false
			g.stylesheets false
			g.javascripts true	# turn this on only for those views that need scripts
			end
		#
		```

- With default generators
```sh
# 5.4
rails generate model Role
# Output:
# Running via Spring preloader in process 9035
#       invoke  active_record
#       create    db/migrate/20190502183242_create_roles.rb
#       create    app/models/role.rb
#       invoke    test_unit
#       create      test/models/role_test.rb
#       create      test/fixtures/roles.yml

# 5.1
rails generate controller Roles
# Output:
# Running via Spring preloader in process 6928
#       create  app/controllers/roles_controller.rb
#       invoke  erb
#       create    app/views/roles
#       invoke  test_unit
#       create    test/controllers/roles_controller_test.rb
#       invoke  helper
#       create    app/helpers/roles_helper.rb
#       invoke    test_unit
#       create      test/helpers/roles_helper_test.rb
#       invoke  assets
#       invoke    coffee
#       create      app/assets/javascripts/roles.js.coffee
#       invoke    scss
#       create      app/assets/stylesheets/roles.css.scss
```

## Commands for Authorization
```sh
rails generate model Features
# Running via Spring preloader in process 6609
#       invoke  active_record
#       create    db/migrate/20190503183607_create_features.rb
#       create    app/models/features.rb
#       invoke    test_unit
#       create      test/models/features_test.rb
#       create      test/fixtures/features.yml

rails generate controller Features
# Running via Spring preloader in process 6659
#       create  app/controllers/features_controller.rb
#       invoke  erb
#       create    app/views/features
#       invoke  test_unit
#       create    test/controllers/features_controller_test.rb
#       invoke  helper
#       create    app/helpers/features_helper.rb
#       invoke    test_unit
#       create      test/helpers/features_helper_test.rb
#       invoke  assets
#       invoke    coffee
#       create      app/assets/javascripts/features.js.coffee
#       invoke    scss
#       create      app/assets/stylesheets/features.css.scss

#===============================================================================
rails generate model Actions
# Running via Spring preloader in process 6719
#       invoke  active_record
#       create    db/migrate/20190503183705_create_actions.rb
#       create    app/models/actions.rb
#       invoke    test_unit
#       create      test/models/actions_test.rb
#       create      test/fixtures/actions.yml

rails generate controller Actions
# Running via Spring preloader in process 6756
#       create  app/controllers/actions_controller.rb
#       invoke  erb
#       create    app/views/actions
#       invoke  test_unit
#       create    test/controllers/actions_controller_test.rb
#       invoke  helper
#       create    app/helpers/actions_helper.rb
#       invoke    test_unit
#       create      test/helpers/actions_helper_test.rb
#       invoke  assets
#       invoke    coffee
#       create      app/assets/javascripts/actions.js.coffee
#       invoke    scss
#       create      app/assets/stylesheets/actions.css.scss

#===============================================================================
rails generate model UserAuthMap
# Running via Spring preloader in process 6829
#       invoke  active_record
#       create    db/migrate/20190503183826_create_user_auth_maps.rb
#       create    app/models/user_auth_map.rb
#       invoke    test_unit
#       create      test/models/user_auth_map_test.rb
#       create      test/fixtures/user_auth_maps.yml

rails generate controller UserAuthMap
# Running via Spring preloader in process 6866
#       create  app/controllers/user_auth_map_controller.rb
#       invoke  erb
#       create    app/views/user_auth_map
#       invoke  test_unit
#       create    test/controllers/user_auth_map_controller_test.rb
#       invoke  helper
#       create    app/helpers/user_auth_map_helper.rb
#       invoke    test_unit
#       create      test/helpers/user_auth_map_helper_test.rb
#       invoke  assets
#       invoke    coffee
#       create      app/assets/javascripts/user_auth_map.js.coffee
#       invoke    scss
#       create      app/assets/stylesheets/user_auth_map.css.scss

rails generate model Actions
# Running via Spring preloader in process 11299
#       invoke  active_record
#       create    db/migrate/20190505101029_create_actions.rb
#       create    app/models/actions.rb
#       invoke    test_unit
#    identical      test/models/actions_test.rb
#    identical      test/fixtures/actions.yml

```
