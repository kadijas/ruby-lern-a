# Items to be done

## 2019-05-09
- [ ] Create base model for all form models, and inherit from them
  - [ ] Block and throw error if save called on these models
- [ ] Recreate Models and Controllers with naming conventions
	```sh
	# Do after checkin, to recover from any overwrites
	# Comment each line as it is done, to track status
	# Models
	rails generate model Action 					# maps to table gen_actions
	rails generate model Feature 					# maps to table gen_features
	rails generate model Roles 						# maps to table gen_roles
	rails generate model UiSections 				# maps to table gen_ui_sections
	rails generate model RoleAuthTemplate 			# maps to table tpl_role_auth_map
	rails generate model UserAuthEntry 				# maps to table user_auth_map
	rails generate model UserRole 					# maps to table user_role_map
	rails generate model UserUiBlacklistEntry 		# maps to table user_uis_blacklist_map
	# rails generate model UserAuthsForm 				# maps to table user_auths_form
	# rails generate model UserForm 					# maps to table user_staff_form
	# rails generate model UserUiBlacklistEntryForm 	# maps to table user_ui_blacklist_form
	# rails generate model VwActionFeature 				# maps to view vw_actions_features


	# Controllers
	rails generate controller actions
	rails generate controller features
	rails generate controller roles
	rails generate controller uisections
	rails generate controller roleauthtemplates
	rails generate controller userauthentries
	rails generate controller userroles
	rails generate controller useruiblacklistentries

	```
  - [ ] Clean up stale migration from earlier non-conventional generates

