# Design notes

## UI
### Fields needed
- staff id
- feature id
- action id
- action name
- status

Include all actions availaible
auth + non-auth

status is dropdown or checkbox

use color coding to identify auth rows
	include color code to indicate changed rows or original status

Sort by Feature code & by Action Code

TODO: 
- post array of objects to rails
- sort array of objects (need custom comparer)
  - (f * 10000) + (a)
  - `24000 + 241 = 24241`
  - `20000 + 003 = 20003`
