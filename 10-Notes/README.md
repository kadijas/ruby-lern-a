# Working Notes
- https://guides.rubyonrails.org/v4.1/getting_started.html
	- Done till step 5.9

## Status matrix

| Controller         | index | show | new | edit | create | update | destroy |
|--------------------|-------|------|-----|------|--------|--------|---------|
| Role               | Y     | Y    | Y   | p    | Y      | P      | NA      |
| Action             | Y     | P    | -   | -    | -      | -      | NA      |
| Feature            | Y     | Y    | -   | Y    | -      | -      | NA      |
| User Auth Map      | Y     | NA   | NA  | Y    | NA     | -      | NA      |
| Role Auth Template | Y     | Y    | NA  | -    | NA     | p      | NA      |


## Links used in debugging
- https://guides.rubyonrails.org/form_helpers.html
- https://apidock.com/rails/ActionView/Helpers/FormTagHelper/text_field_tag
- Chrome extension to pretty view json "JSON Viewer Awesome"


## Generation tips
- Models, use singuler
	- `rails generate model Role`
	- `bin/rails g model Article title:string text:text`
- Controller, use plural and all lower case
	- `rails generate controller roles`
	- `bin/rails g controller articles`
- Name to symbol conversion
	- Upper case chars will be converted to lower case and prefixed with `_`
	- `UserForm` will produce symbol `:user_form`
	- `UserAuthsForm` will produce symbol `:user_auths_form`
	- Generated file name is same as symbol (confirm)
	- TODO: test name conversion for controllers
- Generate DB Schema
	- `rake db:schema:dump`
	- to use in manually generating a migration file
- Generate DB Structure
	- `rake db:structure:dump`
	- DB format script, to run in databse
	- Use with care, include `DROP TABLE IF EXISTS ` for all tables


## Working
- http://0.0.0.0:3000/

## Notes

- **Links**
	- http://ruby-for-beginners.rubymonstas.org/index.html
	- https://ruby-doc.org/core-2.1.5/
	- https://ruby-doc.org/stdlib-2.1.5/
	- https://api.rubyonrails.org/v4.1.6/
	- https://guides.rubyonrails.org/v4.1/
	- https://guides.rubyonrails.org/v4.1/configuring.html
	- Standalone Models
		- https://api.rubyonrails.org/v4.1.6/classes/ActiveModel/Model.html
	- http://pdabrowski.com/blog/rails-advanced-usage-of-content-tag

- **Gem documentation**
	- view the documentation for your installed gems in your browser with the server command:
	- `gem server`
	- access this documentation at http://localhost:8808

- **Further reading**
	- List all availaible versions of a gem
		- `gem list composite_primary_keys -ra`


- **Application environment information**


```
http://0.0.0.0:3000/rails/info/properties

Ruby version	2.1.5-p273 (x86_64-linux)
RubyGems version	2.2.2
Rack version	1.5.5
Rails version	4.1.6
JavaScript Runtime	Node.js (V8)
Active Record version	4.1.6
Action Pack version	4.1.6
Action View version	4.1.6
Action Mailer version	4.1.6
Active Support version	4.1.6
Middleware	

	Rack::Sendfile
	ActionDispatch::Static
	Rack::Lock
	#<ActiveSupport::Cache::Strategy::LocalCache::Middleware:0x0055667ba467f8>
	Rack::Runtime
	Rack::MethodOverride
	ActionDispatch::RequestId
	Rails::Rack::Logger
	ActionDispatch::ShowExceptions
	ActionDispatch::DebugExceptions
	ActionDispatch::RemoteIp
	ActionDispatch::Reloader
	ActionDispatch::Callbacks
	ActiveRecord::Migration::CheckPending
	ActiveRecord::ConnectionAdapters::ConnectionManagement
	ActiveRecord::QueryCache
	ActionDispatch::Cookies
	ActionDispatch::Session::CookieStore
	ActionDispatch::Flash
	ActionDispatch::ParamsParser
	Rack::Head
	Rack::ConditionalGet
	Rack::ETag

Application root	/70_Current/Learning/RubyOnRails-A
Environment	development
Database adapter	mysql2
Database schema version	20180513175617
```
