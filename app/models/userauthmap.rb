class Userauthmap < ActiveRecord::Base
	self.table_name = "user_auth_map"
	belongs_to :staff, foreign_key: "staff_id"

	#
	# Commenting out all keys for now, till understanding is good.
	#
	# self.primary_keys = :staff_id, :action_id, :feature_id

	# # Should this table should use 'has_and_belongs_to_many' relation
	# # https://guides.rubyonrails.org/v4.1/association_basics.html#has-and-belongs-to-many-association-reference
	# has_many: :staffs, foreign_key: "staff_id"
	# has_many: :actions, :foreign_key => [:action_id, :feature_id]
	# has_many: :features, :through actions, foreign_key: "feature_id"
end

# for 'Composite Primary Keys for ActiveRecords' syntax, refer file 
# tmp/_downloads/composite_primary_keys-7.0.10/README.rdoc
#
# self.primary_keys = :user_id, :group_id
# has_many :statuses, :class_name => 'MembershipStatus', :foreign_key => [:user_id, :group_id]
#
