class VwActionFeature < ActiveRecord::Base
  self.table_name = "vw_actions_features"
  def readonly?
    true
  end
end

# VwActionFeature Columns:
#	action_id
#	feature_id
#	action_code
#	action_name
#	action_desc
#	status
#	feature_code
#	feature_name
#	feature_desc
#	feature_status
