class Feature < ActiveRecord::Base
	self.table_name = "gen_features"
	self.primary_key = "feature_id"

	has_many :actions, foreign_key: "feature_id"
	# has_many: :user_auth_maps, :through actions, foreign_key: "feature_id"
end
