class RoleAuthTemplate < ActiveRecord::Base
  self.table_name = "tpl_role_auth_map"
  has_one :role, foreign_key: "role_id"
end
