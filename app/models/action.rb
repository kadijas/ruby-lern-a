class Action < ActiveRecord::Base
	self.table_name = "gen_actions"
	self.primary_key = "action_id"

	# Check syntax for v 4.1.6
	belongs_to :feature, foreign_key: "feature_id"
	# has_many: :user_auth_maps, foreign_key: "action_id"
end
