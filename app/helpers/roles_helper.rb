module RolesHelper

def testMethodOuter
    @htagO = "RolesHelper.testMethodOuter <== CALLED"
  end

  class RoleHelperOne
		def testMethodInner
			@htagI = "RolesHelper.RoleHelperOne.testMethodInner <== CALLED"
		end
	end
	
end
