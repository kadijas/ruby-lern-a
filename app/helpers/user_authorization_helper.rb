module UserAuthorizationHelper

  # Models are at the bottom of this file
  #	UserForm
  #	UserAuthsForm
  #	UserUiBlacklistEntryForm
  #

  # Implemented using standalone Models
  def getAllActionWithUserAssigns(pStaff)
    @SU = UserForm.new do |su|
      su.staff_id = pStaff.id
      su.staffnum = pStaff.staffnum
      su.full_name = pStaff.fn << " " << pStaff.mi << " " << pStaff.ln
      su.nickname = pStaff.pn
      su.mphone = pStaff.mphone
      su.email = pStaff.email
      su.ssn = pStaff.ssn
      su.status = pStaff.ststatus
      su.add_by = -1
      su.add_on = DateTime.now + 10.year
      su.edit_by = -1
      su.edit_on = DateTime.now + 10.year
    end
    # NOTE: If action is removed from system after assigning to user, that action will not appear in the list of actions.

    # All actions defined in the system
    @allActions = VwActionFeature.where("status = :status_parm", { status_parm: 1 }).order("action_id ASC")

    # All authorized actions for user
    @auths = Userauthmap.where("staff_id = :staff_id_parm AND status = :status_parm", { staff_id_parm: pStaff.id, status_parm: 1 }).readonly.order("action_id ASC")

    # List of authorizations, subset to merge with all-action list
    @allowed = Hash.new(0)
    @auths.each { |e| @allowed[e.action_id] = e.status }

    puts "User #{pStaff.id} has #{@allowed.length} actions authorized, out of #{@allActions.length} actions"

	# limit records for debugging
	@recLimit = 5
    # LEFT OUTER JOIN OF @allActions and @auths
    @userStaffID = pStaff.id
	@allActions.each { |vAction|
		# break if @recLimit < 1 
		# @recLimit -= 1
      @currStatus = @allowed[vAction.action_id]
      @SU.user_auths_form.new { |uAE|
        uAE.staff_id = @userStaffID
        uAE.action_id = vAction.action_id
        uAE.feature_id = vAction.feature_id
        uAE.action_code = vAction.action_code
        uAE.action_name = vAction.action_name
        uAE.action_desc = vAction.action_desc
        uAE.status_curr = @currStatus
        uAE.status_new = @currStatus
        uAE.add_by = -1
        uAE.add_on = DateTime.now + 10.year
        uAE.edit_by = -1
        uAE.edit_on = DateTime.now + 10.year
      }
    }
    return @SU
  end

  # NOT Used anymore. Review and delete sak @ 2019-05-09 02:35:59
  def getAllActionWithUserAssignsArray(pStaff)

    # NOTE: If action is removed from system after assigning to user, that action will not appear in the list of actions.

    # TODO: use view to include feature name too
    # All actions defined in the system
    @allActions = Action.where("status = :status_parm", { status_parm: 1 }).readonly.order("action_id ASC")

    # All authorized actions for user
    @auths = Userauthmap.where("staff_id = :staff_id_parm AND status = :status_parm", { staff_id_parm: pStaff.id, status_parm: 1 }).readonly.order("action_id ASC")

    # List of authorizations, subset to merge with all-action list
    @allowed = Hash.new(0)
    @auths.each { |e| @allowed[e.action_id] = e.status }

    puts "User #{pStaff.id} has #{@allowed.length} authorized, out of #{@allActions.length} actions"

    # LEFT OUTER JOIN OF @allActions and @auths
    @arrEditList = Array.new()
    @allActions.each { |vAction|
      @arrEditList << { :a_id => vAction.action_id, :f_id => vAction.feature_id, :a_code => vAction.action_code, :a_nm => vAction.action_name, :a_dsc => vAction.action_desc, :status => @allowed[vAction.action_id] }
    }

    return @arrEditList
  end

  #-------------------------------------------------------------------------------#
  #                                                                               #
  # Standalone Models                                                             #
  #                                                                               #
  #-------------------------------------------------------------------------------#

  #-------------------------------------------------------------------------------#
  # UserForm :user_form                                                           #
  #-------------------------------------------------------------------------------#
  class UserForm < ActiveRecord::Base
    self.table_name = "user_staff_form"
    has_many :user_auths_form, foreign_key: "staff_id"

    accepts_nested_attributes_for :user_auths_form,
                                  :allow_destroy => false,
                                  :reject_if => :all_blank

    # TODO: add relation to user_ui_blacklist_entry_form via user_auths_form

    # Test if this blocks manual create of records
    def readonly?
      true
    end

    # create_table "user_staff_form", primary_key: "staff_id", force: true do |t|
    #   t.string "staffnum", limit: 20
    #   t.string "full_name"
    #   t.string "nickname"
    #   t.string "mphone"
    #   t.string "email"
    #   t.string "ssn"
    #   t.integer "status", default: 0, null: false
    #   t.integer "add_by", default: 0, null: false
    #   t.timestamp "add_on", null: false
    #   t.integer "edit_by", default: 0, null: false
    #   t.timestamp "edit_on", null: false
    # end
  end

  #-------------------------------------------------------------------------------#
  # UserAuthsForm :user_auths_form                                                #
  #-------------------------------------------------------------------------------#
  class UserAuthsForm < ActiveRecord::Base
    self.table_name = "user_auths_form"
    belongs_to :user_form, foreign_key: "staff_id"
    # has_many :user_ui_blacklist_entry_form, foreign_key: "action_id"

    # accepts_nested_attributes_for :user_ui_blacklist_entry_form,
    #                               :allow_destroy => false

    def readonly?
      true
    end

	# FIX: to resolve the 'to_json' error 'nil is not a symbol'
	#	when no primary key is defined on the table
    def attributes
      super.except(nil)
	end
	
	# TODO: Add feature description, will be helpful in UI
    # create_table "user_auths_form", id: false, force: true do |t|
    #   t.integer "action_id", null: false
    #   t.integer "staff_id", null: false
    #   t.integer "feature_id", null: false
    #   t.string "action_code", limit: 20, null: false
    #   t.string "action_name", limit: 60, null: false
    #   t.string "action_desc"
    #   t.integer "status_curr", default: 0, null: false
    #   t.integer "status_new", default: 0, null: false
    #   t.integer "add_by", default: 0, null: false
    #   t.timestamp "add_on", null: false
    #   t.integer "edit_by", default: 0, null: false
    #   t.timestamp "edit_on", null: false
    # end
  end

  #-------------------------------------------------------------------------------#
  # UserUiBlacklistEntryForm :user_ui_blacklist_entry_form                        #
  #-------------------------------------------------------------------------------#
  class UserUiBlacklistEntryForm < ActiveRecord::Base
    self.table_name = "user_ui_blacklist_form"
    belongs_to :user_auths_form, foreign_key: "action_id"

    # Test if this blocks manual create of records
    def readonly?
      true
    end

    # FIX: to resolve the 'to_json' error 'nil is not a symbol'
    #	when no primary key is defined on the table
    def attributes
      super.except(nil)
    end

    # create_table "user_ui_blacklist_form", id: false, force: true do |t|
    #   t.integer "uis_id", null: false
    #   t.integer "staff_id", null: false
    #   t.integer "action_id", null: false
    #   t.integer "feature_id", null: false
    #   t.string "uis_code", limit: 20, null: false
    #   t.string "uis_name", limit: 60, null: false
    #   t.string "uis_desc"
    #   t.integer "status_curr", default: 0, null: false
    #   t.integer "status_new", default: 0, null: false
    #   t.integer "add_by", default: 0, null: false
    #   t.timestamp "add_on", null: false
    #   t.integer "edit_by", default: 0, null: false
    #   t.timestamp "edit_on", null: false
    # end

  end
end
