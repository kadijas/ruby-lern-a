module SelectOptionsHelper

	private

	# Entity record status options
  def statusOptions
    statusOptions = [["Active", 1], ["Inactive", 0], ["Undefined", 3]]
    # Also works
    # statusOptions = Array[["Active", 1], ["Inactive" , 0], ["Undefined", 3]]
  end

  # Options for action and ui section authorization
  def authOptions
    authOptions = [["Allow", 1], ["Deny", 0], ["Review", 3]]
  end
end

