class FeaturesController < ApplicationController
  include SelectOptionsHelper
  # Routes:
  #      features GET    /features(.:format)          features#index
  #               POST   /features(.:format)          features#create
  #   new_feature GET    /features/new(.:format)      features#new
  #  edit_feature GET    /features/:id/edit(.:format) features#edit
  #       feature GET    /features/:id(.:format)      features#show
  #               PATCH  /features/:id(.:format)      features#update
  #               PUT    /features/:id(.:format)      features#update
  #               DELETE /features/:id(.:format)      features#destroy

  def index
    getActionCode
    @features = Feature.includes(:actions).all
	# puts ("take a break")
	# render json: @features.to_json(root: true, include: :actions)
  end

  def show
    getActionCode
    # render plain: params[:action].inspect
    @feature = Feature.find(params[:id])
  end

  def new
    getActionCode
    @feature = Feature.new
  end

  def edit
    getActionCode
	# render plain: params[:action].inspect
	@StatusArray = statusOptions
    @feature = Feature.find(params[:id])
    # puts @feature
  end

  def create
    puts "In RolesController => create"
    getActionCode
    # render plain: params[:role].inspect

    @feature = Feature.new(action_params)

    # The render method is used so that the @article object
    # is passed back to the new template when it is rendered.
    # This rendering is done within same request as the form submission
    # whereas the redirect_to will tell browser to issue another request.
    if @Feature.save
      redirect_to @feature
    else
      render "new"
    end
  end

  def update
	getActionCode
	@NewStatus = params[:status]
    @feature = Feature.find(params[:id])
    if @feature.update_attributes(feature_params)
      redirect_to :action => "show", :id => @feature
    else
      puts "Error saving"
      # Show the error message
    end
  end

  private

  def feature_params
    params.require(:feature).permit(:feature_id, :feature_code, :feature_name, :feature_desc, :status)
  end

end
