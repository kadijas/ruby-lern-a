class RolesController < ApplicationController
  require "securerandom"
  # Routes:
  #     roles GET    /roles(.:format)          roles#index
  #           POST   /roles(.:format)          roles#create
  #  new_role GET    /roles/new(.:format)      roles#new
  # edit_role GET    /roles/:id/edit(.:format) roles#edit
  #      role GET    /roles/:id(.:format)      roles#show
  #           PATCH  /roles/:id(.:format)      roles#update
  #           PUT    /roles/:id(.:format)      roles#update
  #           DELETE /roles/:id(.:format)      roles#destroy
  #
  # A frequent practice is to place the standard CRUD actions in each controller
  # in the following order:
  #    - index
  #    - show
  #    - new
  #    - edit
  #    - create
  #    - update
  #    - destroy

  include RolesHelper

  def index
	getActionCode
	# puts "#{getActionCode()} << Called from RolesController.index"

	# @lsTag = SecureRandom.hex(24)
	# puts "Using tag => #{@lsTag}"

	# puts testMethodOuter
	# @MyHelper = RoleHelperOne.new
	# puts @MyHelper.testMethodInner

	# puts "Testing Inheritance ..."
	# testPrivate
	# testProtected
	puts ("take a break")

    @roles = Role.all
  end

  def show
	puts getActionCode
	
    # render plain: params[:id].inspect
    # render plain: params[:role].inspect
    @role = Role.find(params[:id])
  end

  def new
    getActionCode
    @role = Role.new
  end

  def edit
    getActionCode
    @role = Role.find(params[:id])
  end

  def create
    puts "In RolesController => create"
    getActionCode
    # render plain: params[:role].inspect

    @role = Role.new(role_params)

    # The render method is used so that the @article object
    # is passed back to the new template when it is rendered.
    # This rendering is done within same request as the form submission
    # whereas the redirect_to will tell browser to issue another request.
    if @role.save
      redirect_to @role
    else
      render "new"
    end
  end

  def update
    getActionCode
    @role = Role.find(params[:id])
  end

  private

  def role_params
    params.require(:role).permit(:role_id, :role_name, :role_desc, :status)
  end

end
