class ActionsController < ApplicationController
  # Routes:
  #       actions GET    /actions(.:format)           actions#index
  #               POST   /actions(.:format)           actions#create
  #    new_action GET    /actions/new(.:format)       actions#new
  #   edit_action GET    /actions/:id/edit(.:format)  actions#edit
  #        action GET    /actions/:id(.:format)       actions#show
  #               PATCH  /actions/:id(.:format)       actions#update
  #               PUT    /actions/:id(.:format)       actions#update
  #               DELETE /actions/:id(.:format)       actions#destroy

  def index
    @actions = Action.all.includes(:feature)
	# puts ("take a break")
    # render json: @actions.to_json(root: true, include: :feature)
  end

  def show
	# Get all, and check to ensure only one is returned
	@action_all = Action.includes(:feature).where("action_id = :aid", { :aid => params[:id] }).all
	if @action_all.length > 1
		puts "ERROR: more than one record returned. Expected 1 got #{@action_all.length}"
	end
	@action = @action_all[0]
	
	# Inspect input to the view
	# puts ("take a break!")
	render json: @action.to_json(root: true, include: :feature)
  end

  def new
    @action = Action.new
  end

  def edit
    # render plain: params[:action].inspect
    @action = Action.find(params[:id]) #.includes()
    @feature = Feature.find(@action.feature_id)
  end

  def create
    @action = Action.new(action_params)

    # The render method is used so that the @article object
    # is passed back to the new template when it is rendered.
    # This rendering is done within same request as the form submission
    # whereas the redirect_to will tell browser to issue another request.
    if @action.save
      redirect_to @action
    else
      render "new"
    end
  end

  private

  def action_params
    params.require(:action).permit(:action_id, :feature_id, :action_code, :action_name, :action_desc, :status)
  end
end
