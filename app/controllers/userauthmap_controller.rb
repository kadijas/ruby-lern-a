class UserauthmapController < ApplicationController
include UserAuthorizationHelper
include SelectOptionsHelper

# Routes:	
# userauthmap_index GET    /userauthmap(.:format)          userauthmap#index
#                   POST   /userauthmap(.:format)          userauthmap#create
#   new_userauthmap GET    /userauthmap/new(.:format)      userauthmap#new
#  edit_userauthmap GET    /userauthmap/:id/edit(.:format) userauthmap#edit
#       userauthmap GET    /userauthmap/:id(.:format)      userauthmap#show
#                   PATCH  /userauthmap/:id(.:format)      userauthmap#update
#                   PUT    /userauthmap/:id(.:format)      userauthmap#update
#                   DELETE /userauthmap/:id(.:format)      userauthmap#destroy
	def index
    getActionCode
	@staffs = Staff.all
	end

	# def show
	# 	# Not really used
    # 	getActionCode
	# 	@staff = Staff.find(params[:id])
	# 	@actionListEdit = getAllActionWithUserAssignsArray params[:id]
	# end

  def edit
    getActionCode
	@AuthStatusArray = authOptions
	@staff = Staff.find(params[:id])
	@userAuthActions = getAllActionWithUserAssigns @staff
	# for debugging
	# render json: @userAuthActions.to_json(root: true, include: :user_auths_form)

  end

  def update
	getActionCode
	# render json: params[:user_auths_form_attributes].to_json
	@staff = UserForm.new(userauthmap_params)
	# params[:user_authorization_helper_user_form][:user_auths_form_attributes].length
	# puts "take a break"
	params[:user_authorization_helper_user_form][:user_auths_form_attributes].each do |cr|
		# TODO: Using array position is brittle, research better option
		@staff.user_auths_form.new(cr[1])
	end
	puts "Add #{@staff.user_auths_form.length} entries to map."
    render json: @staff.to_json(root: true, include: :user_auths_form)
  end

  private

  def userauthmap_params
	params.require(:user_authorization_helper_user_form).permit(:staff_id, :staffnum, :full_name, :nickname, :mphone, :email, :ssn, :status, :add_by, :add_on, :edit_by, :edit_on, :action_id, :staff_id, :feature_id, :action_code, :action_name, :action_desc, :status_curr, :status_new)
  end

end
