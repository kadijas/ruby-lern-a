class WelcomeController < ApplicationController
  def index
    puts "Take a break."
    dumpRoutes
  end

  private

  def dumpRoutes
    @routeData = Rails.application.routes.routes
    @counter = 0
    File.open("10-Notes/dumps/app-routes-raw.txt", "w") { |f|
      @routeData.each { |vRT|
        @counter += 1
        if @counter = 40
          puts "Take a break."
        end

        f.puts vRT.name.to_json(root: true).to_s
        # vRT.path.to_json(root: true)
        # stack level too deep
        # vendor/bundle/ruby/2.1.0/gems/actionpack-4.1.6/lib/action_dispatch/middleware/reloader.rb:79
        # vRT.app.to_json(root: true)
        # not opened for reading
        f.puts vRT.constraints.to_json(root: true).to_s
        f.puts vRT.defaults.to_json(root: true).to_s
        f.puts vRT.precedence.to_json(root: true).to_s
        f.puts "done one route..."
      }
    }
    puts "Done writing route info"
    # File
  end
end
