class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # TEMP: testing function delete after testing or 15 days
  # 2019-05-07
  def putPayload
    raw_payload = {
      :controller => self.class.name,
      :action => self.action_name,
      :method => request.method,
      :params => request.filtered_parameters,
      :format => request.format.try(:ref),
      :path => (request.fullpath rescue "unknown"),
    }
    puts raw_payload
  end

  # Should be usable by children
  protected

  # Function to build action code, to use in authorization
  def getActionCode
    @ReqMethod = params[:_method] || request.method
    @ActionCode = "f_#{params[:controller]}-a_#{params["action"]}-#{@ReqMethod[0..1]}".downcase
	puts "#{@ActionCode} << ACTION_CODE << ApplicationController.getActionCode()"
	return @ActionCode
  end

  def testProtected
	puts "testProtected << Called"
  end

  private
  def testPrivate
    puts "testPrivate << Called"
  end

end
