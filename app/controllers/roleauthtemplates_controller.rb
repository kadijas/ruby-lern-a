class RoleauthtemplatesController < ApplicationController
  # Routes:
  #     roleauthtemplates GET    /roleauthtemplates(.:format)          roleauthtemplates#index
  #                       POST   /roleauthtemplates(.:format)          roleauthtemplates#create
  #  new_roleauthtemplate GET    /roleauthtemplates/new(.:format)      roleauthtemplates#new
  # edit_roleauthtemplate GET    /roleauthtemplates/:id/edit(.:format) roleauthtemplates#edit
  #      roleauthtemplate GET    /roleauthtemplates/:id(.:format)      roleauthtemplates#show
  #                       PATCH  /roleauthtemplates/:id(.:format)      roleauthtemplates#update
  #                       PUT    /roleauthtemplates/:id(.:format)      roleauthtemplates#update
  #                       DELETE /roleauthtemplates/:id(.:format)      roleauthtemplates#destroy

  def index
    getActionCode

	# display list of all roles in system
    @roles = Role.all
  end

  def show
	getActionCode
    puts ("take a break")

	# display list of all auths for the given role

	@roleTemplates = RoleAuthTemplate.where("role_id = :role_id", { :role_id => params[:id] }).all
	
	# for debugging only
	# render json: @roleTemplates.to_json
	# render plain: @roleTemplates.inspect
  end

  def new
    getActionCode
  end

  def edit
    getActionCode
  end

  def create
    getActionCode
  end

  def update
    getActionCode
  end

  def destroy
    getActionCode
  end
end
