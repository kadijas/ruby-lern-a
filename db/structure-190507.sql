-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: appts
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MAINT_SESSIONS`
--

DROP TABLE IF EXISTS `MAINT_SESSIONS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MAINT_SESSIONS` (
  `MSID` int(10) unsigned NOT NULL,
  `TOKEN` varchar(60) NOT NULL COMMENT 'Token shared with client, and will be used to match request to session.',
  `DEVICE_SIG` text COMMENT 'Device identifying hash, to uniquely link a device to a session. Helps avoid session hijacks.',
  `STAFF_ID` int(11) NOT NULL COMMENT 'Link to system ID of the user.',
  `ROLE` int(11) NOT NULL DEFAULT '-1' COMMENT 'Not of significance for now, as users can be in multiple roles simultaneously.',
  `ISSUE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_USED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `STATUS` int(11) NOT NULL COMMENT 'Current status of the session. Link to values from enum ''x-y-z''',
  `PAYLOAD` text COMMENT 'JSON formatted profile data of the current user. To minimize db calls.',
  PRIMARY KEY (`MSID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gen_actions`
--

DROP TABLE IF EXISTS `gen_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_actions` (
  `action_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feature_id` int(10) unsigned NOT NULL,
  `action_code` varchar(20) NOT NULL COMMENT 'Extra long code field to guarantee uniqueness. Will be a combination of controller and Action names. TODO: Use generated code.',
  `action_name` varchar(60) NOT NULL COMMENT 'Combination of Controller, Action and HTTP Method.',
  `action_desc` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`action_id`),
  UNIQUE KEY `action_code` (`action_code`),
  UNIQUE KEY `action_name` (`action_name`),
  UNIQUE KEY `UNIQ_FEATURE_ACTION-CODES` (`action_id`,`feature_id`),
  KEY `FKgen_action525764` (`feature_id`),
  CONSTRAINT `FKgen_action525764` FOREIGN KEY (`feature_id`) REFERENCES `gen_features` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Listing of all actions available for each feature in the system.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gen_features`
--

DROP TABLE IF EXISTS `gen_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_features` (
  `feature_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feature_code` varchar(20) NOT NULL,
  `feature_name` varchar(60) NOT NULL,
  `feature_desc` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`feature_id`),
  UNIQUE KEY `feature_name` (`feature_name`),
  UNIQUE KEY `feature_code` (`feature_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Listing of all available features (Controllers) in the system.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gen_roles`
--

DROP TABLE IF EXISTS `gen_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_roles` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  `role_desc` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Roles in the system that can be assigned to a user, for managing system access.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gen_ui_sections`
--

DROP TABLE IF EXISTS `gen_ui_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gen_ui_sections` (
  `uis_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action_id` int(10) unsigned NOT NULL,
  `feature_id` int(10) unsigned NOT NULL,
  `uis_code` varchar(20) NOT NULL COMMENT 'Make this human readable, to be used directly in code.',
  `uis_name` varchar(60) NOT NULL,
  `uis_desc` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uis_id`,`action_id`),
  UNIQUE KEY `uis_name` (`uis_name`),
  UNIQUE KEY `uis_code` (`uis_code`),
  KEY `FKgen_ui_sec153353` (`action_id`),
  KEY `FKgen_ui_sec970062` (`feature_id`),
  CONSTRAINT `FKgen_ui_sec153353` FOREIGN KEY (`action_id`) REFERENCES `gen_actions` (`action_id`),
  CONSTRAINT `FKgen_ui_sec970062` FOREIGN KEY (`feature_id`) REFERENCES `gen_features` (`feature_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='List of all non-default UI Sections for actions (View) of each feature (Controller) in the system.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staffs`
--

DROP TABLE IF EXISTS `staffs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staffs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fn` varchar(255) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `mi` varchar(255) DEFAULT NULL,
  `pn` varchar(255) DEFAULT NULL,
  `title` tinyint(4) DEFAULT NULL,
  `addone` varchar(255) DEFAULT NULL,
  `addtwo` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `mphone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ssn` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `ststatus` tinyint(4) DEFAULT '1',
  `staffnum` varchar(20) DEFAULT NULL,
  `ophone` varchar(255) DEFAULT NULL,
  `password_digest` varchar(255) DEFAULT NULL,
  `is_user` tinyint(4) DEFAULT NULL,
  `is_prov` tinyint(4) DEFAULT '0',
  `is_rm` tinyint(4) DEFAULT '0',
  `is_hyg` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tpl_role_auth_map`
--

DROP TABLE IF EXISTS `tpl_role_auth_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tpl_role_auth_map` (
  `role_id` int(10) unsigned NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `feature_id` int(10) unsigned NOT NULL,
  `action_name` varchar(60) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`,`action_id`),
  KEY `FKtpl_role_a325011` (`action_id`),
  KEY `FKtpl_role_a798404` (`feature_id`),
  KEY `FKtpl_role_a877886` (`action_name`),
  CONSTRAINT `FKtpl_role_a325011` FOREIGN KEY (`action_id`) REFERENCES `gen_actions` (`action_id`),
  CONSTRAINT `FKtpl_role_a379612` FOREIGN KEY (`role_id`) REFERENCES `gen_roles` (`role_id`),
  CONSTRAINT `FKtpl_role_a798404` FOREIGN KEY (`feature_id`) REFERENCES `gen_features` (`feature_id`),
  CONSTRAINT `FKtpl_role_a877886` FOREIGN KEY (`action_name`) REFERENCES `gen_actions` (`action_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Template for default mapping of features and actions assigned to a role.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_auth_map`
--

DROP TABLE IF EXISTS `user_auth_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_auth_map` (
  `staff_id` int(11) NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `feature_id` int(10) unsigned NOT NULL,
  `action_name` varchar(60) NOT NULL COMMENT 'Combination of Controller, Action and HTTP Method. To enable dynamic query of authorizations.',
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`staff_id`,`action_id`),
  UNIQUE KEY `user_auth_map` (`staff_id`,`action_name`),
  KEY `FKuser_auth_732366` (`action_id`),
  KEY `FKuser_auth_792099` (`action_name`),
  KEY `FKuser_auth_115808` (`feature_id`),
  CONSTRAINT `FKuser_auth_115808` FOREIGN KEY (`feature_id`) REFERENCES `gen_features` (`feature_id`),
  CONSTRAINT `FKuser_auth_732366` FOREIGN KEY (`action_id`) REFERENCES `gen_actions` (`action_id`),
  CONSTRAINT `FKuser_auth_792099` FOREIGN KEY (`action_name`) REFERENCES `gen_actions` (`action_name`),
  CONSTRAINT `FKuser_auth_950632` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Mapping of all actions authorized for a user in the system.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_role_map`
--

DROP TABLE IF EXISTS `user_role_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role_map` (
  `staff_id` int(11) NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `is_primary` int(4) NOT NULL DEFAULT '0' COMMENT 'Flag to indicate if this is the primary role of the user, or additional role. 0 = Addl, 1 = Primary.',
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`staff_id`,`role_id`),
  KEY `FKuser_role_321036` (`role_id`),
  CONSTRAINT `FKuser_role_321036` FOREIGN KEY (`role_id`) REFERENCES `gen_roles` (`role_id`),
  CONSTRAINT `FKuser_role_94996` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Mapping for Users and roles. Identifies all roles a user has been assigned to in the system.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_uis_blacklist_map`
--

DROP TABLE IF EXISTS `user_uis_blacklist_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_uis_blacklist_map` (
  `staff_id` int(11) NOT NULL,
  `uis_id` int(10) unsigned NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `feature_id` int(10) unsigned NOT NULL,
  `uis_code` varchar(20) NOT NULL COMMENT 'Text code more readable in code than numeric code. To enable dynamic query of authorizations.',
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`staff_id`,`uis_id`,`action_id`),
  UNIQUE KEY `uis_code` (`uis_code`),
  UNIQUE KEY `user_uis_blacklist_map` (`staff_id`,`uis_code`),
  KEY `FKuser_uis_b270849` (`uis_id`,`action_id`),
  KEY `FKuser_uis_b326039` (`feature_id`),
  CONSTRAINT `FKuser_uis_b160864` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`id`),
  CONSTRAINT `FKuser_uis_b270849` FOREIGN KEY (`uis_id`, `action_id`) REFERENCES `gen_ui_sections` (`uis_id`, `action_id`),
  CONSTRAINT `FKuser_uis_b326039` FOREIGN KEY (`feature_id`) REFERENCES `gen_features` (`feature_id`),
  CONSTRAINT `FKuser_uis_b675482` FOREIGN KEY (`uis_code`) REFERENCES `gen_ui_sections` (`uis_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='List of all blacklisted UI Sections of Actions (Views) for a user. These will be used to block rendering of those sections in the UI. And possibly in the back-end to block data from the blacklisted sections.';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-07 20:38:22
