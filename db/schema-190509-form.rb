# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do
  create_table "user_auths_form", id: false, force: true do |t|
    t.integer "action_id", null: false
    t.integer "staff_id", null: false
    t.integer "feature_id", null: false
    t.string "action_code", limit: 20, null: false
    t.string "action_name", limit: 60, null: false
    t.string "action_desc"
    t.integer "status_curr", default: 0, null: false
    t.integer "status_new", default: 0, null: false
    t.integer "add_by", default: 0, null: false
    t.timestamp "add_on", null: false
    t.integer "edit_by", default: 0, null: false
    t.timestamp "edit_on", null: false
  end

  add_index "user_auths_form", ["action_code"], name: "action_code", unique: true, using: :btree
  add_index "user_auths_form", ["action_id", "feature_id"], name: "UNIQ_FEATURE_ACTION-CODES", unique: true, using: :btree
  add_index "user_auths_form", ["action_name"], name: "action_name", unique: true, using: :btree
  add_index "user_auths_form", ["staff_id"], name: "FKuser_auths433222", using: :btree

  create_table "user_staff_form", primary_key: "staff_id", force: true do |t|
    t.string "staffnum", limit: 20
    t.string "full_name"
    t.string "nickname"
    t.string "mphone"
    t.string "email"
    t.string "ssn"
    t.integer "status", default: 0, null: false
    t.integer "add_by", default: 0, null: false
    t.timestamp "add_on", null: false
    t.integer "edit_by", default: 0, null: false
    t.timestamp "edit_on", null: false
  end

  create_table "user_ui_blacklist_form", id: false, force: true do |t|
    t.integer "uis_id", null: false
    t.integer "staff_id", null: false
    t.integer "action_id", null: false
    t.integer "feature_id", null: false
    t.string "uis_code", limit: 20, null: false
    t.string "uis_name", limit: 60, null: false
    t.string "uis_desc"
    t.integer "status_curr", default: 0, null: false
    t.integer "status_new", default: 0, null: false
    t.integer "add_by", default: 0, null: false
    t.timestamp "add_on", null: false
    t.integer "edit_by", default: 0, null: false
    t.timestamp "edit_on", null: false
  end

  add_index "user_ui_blacklist_form", ["action_id", "staff_id"], name: "FKuser_ui_bl485438", using: :btree
  add_index "user_ui_blacklist_form", ["uis_code"], name: "uis_code", unique: true, using: :btree
  add_index "user_ui_blacklist_form", ["uis_name"], name: "uis_name", unique: true, using: :btree
end
	