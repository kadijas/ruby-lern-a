# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "MAINT_SESSIONS", primary_key: "MSID", force: true do |t|
    t.string    "TOKEN",      limit: 60,              null: false
    t.text      "DEVICE_SIG"
    t.integer   "STAFF_ID",                           null: false
    t.integer   "ROLE",                  default: -1, null: false
    t.timestamp "ISSUE_TIME",                         null: false
    t.timestamp "LAST_USED",                          null: false
    t.integer   "STATUS",                             null: false
    t.text      "PAYLOAD"
  end

  create_table "gen_actions", primary_key: "action_id", force: true do |t|
    t.integer   "feature_id",                         null: false
    t.string    "action_code", limit: 20,             null: false
    t.string    "action_name", limit: 60,             null: false
    t.string    "action_desc"
    t.integer   "status",                 default: 0, null: false
    t.integer   "add_by",                 default: 0, null: false
    t.timestamp "add_on",                             null: false
    t.integer   "edit_by",                default: 0, null: false
    t.timestamp "edit_on",                            null: false
  end

  add_index "gen_actions", ["action_code"], name: "action_code", unique: true, using: :btree
  add_index "gen_actions", ["action_id", "feature_id"], name: "UNIQ_FEATURE_ACTION-CODES", unique: true, using: :btree
  add_index "gen_actions", ["action_name"], name: "action_name", unique: true, using: :btree
  add_index "gen_actions", ["feature_id"], name: "FKgen_action525764", using: :btree

  create_table "gen_features", primary_key: "feature_id", force: true do |t|
    t.string    "feature_code", limit: 20,             null: false
    t.string    "feature_name", limit: 60,             null: false
    t.string    "feature_desc"
    t.integer   "status",                  default: 0, null: false
    t.integer   "add_by",                  default: 0, null: false
    t.timestamp "add_on",                              null: false
    t.integer   "edit_by",                 default: 0, null: false
    t.timestamp "edit_on",                             null: false
  end

  add_index "gen_features", ["feature_code"], name: "feature_code", unique: true, using: :btree
  add_index "gen_features", ["feature_name"], name: "feature_name", unique: true, using: :btree

  create_table "gen_roles", primary_key: "role_id", force: true do |t|
    t.string    "role_name", limit: 60,             null: false
    t.string    "role_desc",                        null: false
    t.integer   "status",               default: 0, null: false
    t.integer   "add_by",               default: 0, null: false
    t.timestamp "add_on",                           null: false
    t.integer   "edit_by",              default: 0, null: false
    t.timestamp "edit_on",                          null: false
  end

  create_table "gen_ui_sections", id: false, force: true do |t|
    t.integer   "uis_id",                            null: false
    t.integer   "action_id",                         null: false
    t.integer   "feature_id",                        null: false
    t.string    "uis_code",   limit: 20,             null: false
    t.string    "uis_name",   limit: 60,             null: false
    t.string    "uis_desc"
    t.integer   "status",                default: 0, null: false
    t.integer   "add_by",                default: 0, null: false
    t.timestamp "add_on",                            null: false
    t.integer   "edit_by",               default: 0, null: false
    t.timestamp "edit_on",                           null: false
  end

  add_index "gen_ui_sections", ["action_id"], name: "FKgen_ui_sec153353", using: :btree
  add_index "gen_ui_sections", ["feature_id"], name: "FKgen_ui_sec970062", using: :btree
  add_index "gen_ui_sections", ["uis_code"], name: "uis_code", unique: true, using: :btree
  add_index "gen_ui_sections", ["uis_name"], name: "uis_name", unique: true, using: :btree

  create_table "staffs", force: true do |t|
    t.string   "fn"
    t.string   "ln"
    t.string   "mi"
    t.string   "pn"
    t.integer  "title",           limit: 1
    t.string   "addone"
    t.string   "addtwo"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "ext"
    t.string   "mphone"
    t.string   "email"
    t.string   "ssn"
    t.integer  "created_by"
    t.datetime "created_at"
    t.integer  "updated_by"
    t.datetime "updated_at"
    t.integer  "ststatus",        limit: 1,  default: 1
    t.string   "staffnum",        limit: 20
    t.string   "ophone"
    t.string   "password_digest"
    t.integer  "is_user",         limit: 1
    t.integer  "is_prov",         limit: 1,  default: 0
    t.integer  "is_rm",           limit: 1,  default: 0
    t.integer  "is_hyg",          limit: 1,  default: 0
  end

  create_table "tpl_role_auth_map", id: false, force: true do |t|
    t.integer   "role_id",                            null: false
    t.integer   "action_id",                          null: false
    t.integer   "feature_id",                         null: false
    t.string    "action_name", limit: 60,             null: false
    t.integer   "status",                 default: 0, null: false
    t.integer   "add_by",                 default: 0, null: false
    t.timestamp "add_on",                             null: false
    t.integer   "edit_by",                default: 0, null: false
    t.timestamp "edit_on",                            null: false
  end

  add_index "tpl_role_auth_map", ["action_id"], name: "FKtpl_role_a325011", using: :btree
  add_index "tpl_role_auth_map", ["action_name"], name: "FKtpl_role_a877886", using: :btree
  add_index "tpl_role_auth_map", ["feature_id"], name: "FKtpl_role_a798404", using: :btree

  create_table "user_auth_map", id: false, force: true do |t|
    t.integer   "staff_id",                           null: false
    t.integer   "action_id",                          null: false
    t.integer   "feature_id",                         null: false
    t.string    "action_name", limit: 60,             null: false
    t.integer   "status",                 default: 0, null: false
    t.integer   "add_by",                 default: 0, null: false
    t.timestamp "add_on",                             null: false
    t.integer   "edit_by",                default: 0, null: false
    t.timestamp "edit_on",                            null: false
  end

  add_index "user_auth_map", ["action_id"], name: "FKuser_auth_732366", using: :btree
  add_index "user_auth_map", ["action_name"], name: "FKuser_auth_792099", using: :btree
  add_index "user_auth_map", ["feature_id"], name: "FKuser_auth_115808", using: :btree
  add_index "user_auth_map", ["staff_id", "action_name"], name: "user_auth_map", unique: true, using: :btree

  create_table "user_role_map", id: false, force: true do |t|
    t.integer   "staff_id",               null: false
    t.integer   "role_id",                null: false
    t.integer   "is_primary", default: 0, null: false
    t.integer   "status",     default: 0, null: false
    t.integer   "add_by",     default: 0, null: false
    t.timestamp "add_on",                 null: false
    t.integer   "edit_by",    default: 0, null: false
    t.timestamp "edit_on",                null: false
  end

  add_index "user_role_map", ["role_id"], name: "FKuser_role_321036", using: :btree

  create_table "user_uis_blacklist_map", id: false, force: true do |t|
    t.integer   "staff_id",                          null: false
    t.integer   "uis_id",                            null: false
    t.integer   "action_id",                         null: false
    t.integer   "feature_id",                        null: false
    t.string    "uis_code",   limit: 20,             null: false
    t.integer   "status",                default: 0, null: false
    t.integer   "add_by",                default: 0, null: false
    t.timestamp "add_on",                            null: false
    t.integer   "edit_by",               default: 0, null: false
    t.timestamp "edit_on",                           null: false
  end

  add_index "user_uis_blacklist_map", ["feature_id"], name: "FKuser_uis_b326039", using: :btree
  add_index "user_uis_blacklist_map", ["staff_id", "uis_code"], name: "user_uis_blacklist_map", unique: true, using: :btree
  add_index "user_uis_blacklist_map", ["uis_code"], name: "uis_code", unique: true, using: :btree
  add_index "user_uis_blacklist_map", ["uis_id", "action_id"], name: "FKuser_uis_b270849", using: :btree

end
