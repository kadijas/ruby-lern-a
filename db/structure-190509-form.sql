-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: appts
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_auths_form`
--

DROP TABLE IF EXISTS `user_auths_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_auths_form` (
  `action_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `feature_id` int(10) unsigned NOT NULL,
  `action_code` varchar(20) NOT NULL COMMENT 'Extra long code field to guarantee uniqueness. Will be a combination of controller and Action names. TODO: Use generated code.',
  `action_name` varchar(60) NOT NULL COMMENT 'Combination of Controller, Action and HTTP Method.',
  `action_desc` varchar(255) DEFAULT NULL,
  `status_curr` int(11) NOT NULL DEFAULT '0' COMMENT 'Copy to track changed records',
  `status_new` int(11) NOT NULL DEFAULT '0' COMMENT 'Copy to track updated records',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`action_id`,`staff_id`),
  UNIQUE KEY `action_code` (`action_code`),
  UNIQUE KEY `action_name` (`action_name`),
  UNIQUE KEY `UNIQ_FEATURE_ACTION-CODES` (`action_id`,`feature_id`),
  KEY `FKuser_auths433222` (`staff_id`),
  CONSTRAINT `FKuser_auths433222` FOREIGN KEY (`staff_id`) REFERENCES `user_staff_form` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Entity to support standalone models, based on ''gen_actions''';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_staff_form`
--

DROP TABLE IF EXISTS `user_staff_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_staff_form` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `staffnum` varchar(20) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `mphone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ssn` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Entity to support standalone models, based on ''staffs''';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_ui_blacklist_form`
--

DROP TABLE IF EXISTS `user_ui_blacklist_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_ui_blacklist_form` (
  `uis_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `feature_id` int(10) unsigned NOT NULL,
  `uis_code` varchar(20) NOT NULL COMMENT 'Make this human readable, to be used directly in code.',
  `uis_name` varchar(60) NOT NULL,
  `uis_desc` varchar(255) DEFAULT NULL,
  `status_curr` int(11) NOT NULL DEFAULT '0' COMMENT 'Copy to track changed records',
  `status_new` int(11) NOT NULL DEFAULT '0' COMMENT 'Copy to track updated records',
  `add_by` int(10) unsigned NOT NULL DEFAULT '0',
  `add_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_by` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uis_id`,`staff_id`,`action_id`),
  UNIQUE KEY `uis_name` (`uis_name`),
  UNIQUE KEY `uis_code` (`uis_code`),
  KEY `FKuser_ui_bl485438` (`action_id`,`staff_id`),
  CONSTRAINT `FKuser_ui_bl485438` FOREIGN KEY (`action_id`, `staff_id`) REFERENCES `user_auths_form` (`action_id`, `staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Entity to support standalone models, based on ''gen_ui_sections''';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-09 13:46:00
